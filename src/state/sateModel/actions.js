import {SHOW_MODAL,HIDE_MODAL} from './const'

export function handleShowModal() {
      return {
          type:SHOW_MODAL,
      }
}

export function handleHideModal() {
    return {
        type:HIDE_MODAL,
    }
}
