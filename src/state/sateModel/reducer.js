import { SHOW_MODAL } from './const'
import { HIDE_MODAL } from './const'

const initState = {
    stateModal: false,
}

function reducer (state = initState, action) {
      switch (action.type) {
          case SHOW_MODAL:
              return {
                  ...state,
                  stateModal: true,
              }
           case HIDE_MODAL:
               return {
                   ...state,
                   stateModal: false,
               }
            default:
                return state   
      }     
}

export default reducer
