import  {useEffect} from 'react'
import axios from 'axios'
import {actions} from '~/state/user'
import { useDispatch } from 'react-redux'
function Provider() {
    const dispatch = useDispatch()
    useEffect(() => {
        const token = localStorage.getItem('token')
        if(!token) return
        axios.get('/api/auth/me')
            .then(res => {
                dispatch(actions.setCurrentUser(res.data))
            })
            .catch(err => {
                console.error(err)
            })
    }, [dispatch])
    return null
}

export default Provider
