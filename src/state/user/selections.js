export function getCurrentUser(state) {
    return state.user.currentUser
}

export function isAuthenticated(state) {
    const currentUser = getCurrentUser(state)
    return !!currentUser
}

export function getUsers(state) {
    return state.user.users
}

export function getUser(state, id) {
    const users = getUsers(state)
    return users[id] || null
}
