import {GET_CURRENT_USER} from './const'

const initStae = {
    user:null,
}

function reducer (state = initStae,action) {
    switch(action.type) {
        case GET_CURRENT_USER:
            const currentUser = action.payload
            return {
                ...state,
                currentUser
            }
        default:
            return state    
    }
} 

export default reducer