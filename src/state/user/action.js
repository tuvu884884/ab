import { GET_CURRENT_USER } from './const'

export function setCurrentUser (user) {
   return {
       type: GET_CURRENT_USER,
       payload: user
   }
}