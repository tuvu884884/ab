import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import {useSelector,useDispatch} from 'react-redux'
import {actions as actionsModel} from '~/state/sateModel'


import Button from '../../package/tuvx-buttuon'
import styles from './AccountPrivew.module.scss'
import { Link } from 'react-router-dom'
import config from 'config'

const defaultFn = () => {}

function AccountPrivew ({
    data = '',
    avatar='',
    nickname='',
    tick=false,
    fullName='',
    followersCount='',
    likesCount='',
    onFollow = defaultFn,
    minWidth = 280,
    apiPath ='',
    isFollowed = '',
}
) {
    const datas = useSelector(state => state.user.currentUser)
    const stateModal = useSelector(state=> state.stateModel.stateModal)
    const dispatch = useDispatch()
    const handleShowModal = ()=> {
       if(stateModal) {
           dispatch(actionsModel.handleHideModal())
       }
       else {
        dispatch(actionsModel.handleShowModal())
       }
    }
    return (
       <div className={styles.wrapper} style={{
           minWidth:minWidth + 'px',
       }}>
           <div className={styles.header}>
                   <Link to={`${config.routes.home}profile/@${nickname}`}>
                        <img className={styles.avatar} src={avatar} alt={''} />
                   </Link>
                    <div>
                        {
                            !isFollowed&&<Button
                            type="border"
                            onClick={()=> datas ? onFollow(data): handleShowModal()}
                             >
                                Follow
                             </Button>
                        }
                    </div>
           </div>
           <div className={styles.info}>
                <h3 className={styles.nickname}>
                <Link to={`${config.routes.home}profile/@${nickname}`}>
                {nickname}
                   </Link>
                    {tick && (
                        <FontAwesomeIcon className={styles.tickIcon} icon={faCheckCircle} />
                    )}
                </h3>
                <p className={styles.fullName}>{fullName}</p>
            </div>
            <div className={styles.analytics}>
                <span className={styles.analyticItem}>
                    <strong>{followersCount}</strong> Followers
                </span>
                <span className={styles.analyticItem}>
                    <strong>{likesCount}</strong> Likes
                </span>
            </div>

       </div>
    )
}

export default AccountPrivew
