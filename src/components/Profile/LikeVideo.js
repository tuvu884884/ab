import React from 'react'
import styles from './Profile.module.scss'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faLock} from '@fortawesome/free-solid-svg-icons'

function LikeVideo({
    nickname='',
}) {
    return (
        <div className={styles.likeVideo}>
            <div className={styles.faLock}>
                <FontAwesomeIcon icon={faLock}/>
            </div>
            <p className={styles.title}>
            Video đã thích của người dùng này ở trạng thái riêng tư
            </p>
            <p className={styles.desc}>
            Các video được thích bởi {nickname} hiện đang ẩn
            </p>
        </div>
    )
}

export default LikeVideo
