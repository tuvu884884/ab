import styles from './Profile.module.scss'
import Button from '../../package/tuvx-buttuon'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLink, faLock } from '@fortawesome/free-solid-svg-icons'
import Videos from './Videos'
import LikeVideo from './LikeVideo'

function Profile ({
    profile = {},
    isVideo ='',
    name = '',
    nickname = '',
    avatar = '',
    desc = '',
    followers = '',
    followings = '',
    likes = '',
    videos = '',
    isFirst = '',
    isFollowed = '',
    handleMouseEnterVideo = ()=>{},
    checkPlaying = ()=>{},
    handleToggleHiden= ()=>{},
    handleToggleShow= ()=>{},
    handleToggleFollow = ()=>{},
}) {
    return (
        <div className={styles.profile}>
            <div className={styles.content}>
                <div className={styles.header}>
                    <div className={styles.share__info}>
                        <div className={styles.avatar}>
                            <img src={avatar} alt=""/>
                        </div>
                        <div className={styles.share__title}>
                             <h2 className={styles.title}>
                              {name}
                             </h2>
                             <h1 className={styles.sub__title}>{nickname}</h1>
                                <Button className={styles.btn__follow} size='s' onClick={()=>handleToggleFollow(profile)}>{isFollowed?'Đang Follow':'Follow'}</Button>
                        </div>
                    </div>
                    <h2 className={styles.count__infos}>
                      <div className={styles.number}>
                          <span>{followers}</span>Followers
                      </div>
                      <div className={styles.number}>
                          <span>{followings}</span> Followings
                      </div>
                      <div className={styles.number}>
                          <span>{likes}</span> Likes
                      </div>
                    </h2>
                    <h2 className={styles.share__desc}>
                     {desc}
                    </h2>
                    <div className={styles.share__link}>
                      <FontAwesomeIcon icon={faLink} className={styles.faLink}/>
                      <div className={styles.link}>
                      www.facebook.com/profile.php?id=100038825663534
                      </div>
                    </div>
                </div>
            <div className={styles.share__main}>
                <div className={styles.video__feed__tap}>
                    <div  onClick={handleToggleShow}
                    className={styles.video}>
                        <span >Video</span>
                    </div>

                    <div className={styles.lock} onClick={handleToggleHiden}>
                        <FontAwesomeIcon icon={faLock}/>
                        <span >Video</span>
                    </div>
                 <div className={styles.line}></div>
                </div>
            </div>
            </div>
        {
          isVideo? ( videos &&  <Videos
             videos = {videos}
             isFirst = {isFirst}
             onMouseEnterVideo= {handleMouseEnterVideo}
             isPlayingVideo= {checkPlaying}
         />):   <LikeVideo
            nickname= {nickname}
        />
        }
        </div>
     )
}

export default Profile
