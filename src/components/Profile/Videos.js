import React from 'react'
import ReactPlayer from 'react-player'
import styles from './Profile.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faThumbsUp} from '@fortawesome/free-regular-svg-icons'
function Videos({
  videos = [],
  isFirst = '',
  onMouseEnterVideo = ()=>{},
  isPlayingVideo = ()=>{},
}) {
    return (
        <div className={styles.videos}>
            {
              videos.map((video,index) => (
                <div className={styles.group__videos} key={video.id}>
                <ReactPlayer
                  width='100%'
                  url={video.file_url}
                  playing= {isPlayingVideo(video)||(isFirst && index===0)}
                  loop
                  onMouseEnter={()=> onMouseEnterVideo(video)}
                />
                <div className={styles.description}>
                {video.description}
                </div>
                <div className={styles.faThumbsUp}>
                  <FontAwesomeIcon icon={faThumbsUp}/>
                  {video.likes_count}
                </div>
            </div>
              ))
            }
        </div>
    )
}

export default Videos
