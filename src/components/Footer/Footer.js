import styles from './Footer.module.scss'
import LogoDark from '~/accssets/img/logo-dark.svg'
import LogoTextDark from '~/accssets/img/logo-text-dark.svg'

function Footer() {
    return (
        <div className={styles.footer}>
            <div className={styles.content__wapper}>
                 <div className={styles.logo}>
                     <img src={LogoDark} className={styles.LogoDark} alt=''/>
                     <img src={LogoTextDark} className={styles.LogoTextDark} alt=''/>
                 </div>
                 <div className={styles.content__colum}>
                     <h4>Công ty</h4>
                     <div>
                     <span>Giới thiệu</span>
                     </div>
                     <div>
                     <span>Bản tin</span>
                     </div>
                     <div>
                     <span>Liên hệ</span>
                     </div>
                     <div>
                     <span>Sự nghiệp</span>
                     </div>
                     <div>
                     <span>ByteDance</span>
                     </div>
                 </div>
                 <div className={styles.content__colum}>
                     <h4>Chương trình</h4>
                     <div>
                     <span>TikTok for Good</span>
                     </div>
                     <div>
                     <span>Advertise</span>
                     </div>
                     <div>
                     <span>Developers</span>
                     </div>
                 </div>
                 <div className={styles.content__colum}>
                     <h4>Hỗ trợ</h4>
                     <div>
                     <span>Trung tâm Trợ giúp   </span>
                     </div>
                     <div>
                     <span>Trung tâm An toàn</span>
                     </div>
                     <div>
                     <span>Creator Portal</span>
                     </div>
                     <div>
                     <span>Hướng dẫn Cộng đồng</span>
                     </div>
                     <div>
                     <span>Minh bạch</span>
                     </div>
                 </div>
                 <div className={styles.content__colum}>
                     <h4>Pháp lý</h4>
                     <div>
                     <span>Tuân thủ pháp luật</span>
                     </div>
                     <div>
                     <span>Điều Khoản Dịch Vụ</span>
                     </div>
                     <div>
                     <span>Chính Sách Quyền Riêng Tư</span>
                     </div>
                 </div>
            </div>
            <div className={styles.footer__bottom__wrapper}>
                <select className={styles.language__selection__form}>
                     <option value='en'>Tiếng Việt (Vietnamese)</option>
                </select>
                <div className={styles.copyright}>
                © 2021 TikTok
                </div>
            </div>
        </div>
    )
}

export default Footer
