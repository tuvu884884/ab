import styles from './Follow.module.scss'
import Button from '../../package/tuvx-buttuon'

import ReactPlayer from 'react-player'
import {
    Link
} from 'react-router-dom'
import config from '~/config'

function UserItem ({
    userItem = '',
    name='',
    nickname='',
    avatar='',
    video='',
    isFirst = '',
    oncMouseEnterAccount = ()=>{},
    onClickNewTap= ()=>{},
    isPlay = false,
    playCurrentPage= '',
}) {
     return (
         <>
           <div className={styles.userItem}
           onMouseEnter = {()=>{
            oncMouseEnterAccount(userItem)
           }}
           >
                <ReactPlayer 
                url={video}
                width={'228px'}
                height={'304px'}
                playing = {(isPlay || isFirst) && playCurrentPage}
                loop
                />
                <Link to={`${config.routes.home}profile/@${nickname}`} target="_target" onClick={onClickNewTap}>
                <div className={styles.userInfo}>
                    <div className={styles.avatar}>
                        <img src={avatar} alt=''/>
                    </div>
                    <div className={styles.info}>
                            <p className={styles.name}>{name}</p>
                            <p className={styles.nickname}>{nickname}</p>
                    </div>
                   <Button size='l'>Follow</Button>
                </div>
                </Link>
             </div>
         </>
     )
}

export default UserItem
