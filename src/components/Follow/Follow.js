import styles from './Follow.module.scss'
import Useritem from './UserItem'

function Follow ({
    isPlay = false,
    isFirst= '',
    suggestedAccount=[],
    handleMouseEnterAccount = '',
    playCurrentPage = '',
    handleClickNewTap = ()=>{},
}) {
    return (
        <div className={styles.follow}>
          {
            suggestedAccount.map((userItem,index) => {
               return (
                <Useritem 
                    userItem = {userItem}
                    key={index}
                    name={userItem.first_name + ' ' + userItem.last_name}
                    nickname={userItem.nickname}
                    avatar={userItem.avatar}
                    video={userItem.popular_post.file_url}
                    oncMouseEnterAccount = {handleMouseEnterAccount}
                    isPlay = {isPlay(userItem)}
                    isFirst = {index === 0 && isFirst}
                    playCurrentPage= {playCurrentPage}
                    onClickNewTap= {handleClickNewTap}
                />
               )
              })
          }
        </div>
    )
}

export default Follow
