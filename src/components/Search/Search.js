import styles from './Search.module.scss'
import UserSearch from './UserSearch'
import {
    Link
} from 'react-router-dom'
import config from '~/config'
function Search({
    usersSearch = [],
}) {
    return (
        <div className={styles.search}>
           {
            usersSearch.map((user)=> {
                return (
                   <Link to={`${config.routes.home}profile/@${user.nickname}`}>
                   <UserSearch
                        key = {user.id}
                        name = {user.last_name + ' ' + user.first_name}
                        nickname = {user.nickname}
                        avatar = {user.avatar}
                        desc = {user.bio}
                    />
                   </Link>
                )
            })
           }
        </div>
    )
}

export default Search
