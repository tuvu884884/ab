import styles from './Search.module.scss'

function UserSearch({
    key = '',
    name ='',
    nickname ='',
    avatar ='',
    desc ='',
}) {
    return (
      <>
        <div className={styles.item}>
            <div className={styles.avatar}>
                <img src={avatar} alt=''/>
            </div>
            <div className={styles.info}>
                <div className={styles.name}>
                    {name}
                </div>
                <div className={styles.nickname}>
                    {nickname}
                </div>
                <div className={styles.desc}>
                    {desc}
                </div>
            </div>
        </div>
      </>
    )
}

export default UserSearch
