import styles from './Authen.module.scss'
import Button from '~/package/tuvx-buttuon'

function SignIn({
    showMedia = ()=>{},
    handleSign = ()=>{},
    username = '',
    onChangeUsername =()=>{},
    password = '',
    onChangePassword =()=>{},
    confirmPassword = '',
    onChangeConfirmPassword = ()=>{},
    errors =''
}) {
    return (
        <div className={styles.login__not__media}>
             <form className={styles.from__login}>
                 <div className={styles.group}>
                     <label forhtml='userName' className={styles.userName}>Email :</label> <br/>
                     <input type='text' placeholder='Username' name='userName' id='userName'
                      value = {username}
                      onChange={onChangeUsername}
                     />
                     <p className={styles.errors}>{errors.email}</p>
                 </div>
                 <div className={styles.group}>
                     <label forhtml='password' className={styles.password}>Password :</label> <br/>
                     <input type='password' placeholder='Password' name='password' id='password'
                         value={password}
                         onChange={onChangePassword}
                     />
                     <p className={styles.errors}>{errors.password}</p>
                 </div>
                 <div className={styles.group}>
                     <label forhtml='password' className={styles.password}>Confirm Password :</label> <br/>
                     <input type='password' placeholder='Password' name='password' id='password'
                          value={confirmPassword}
                          onChange={onChangeConfirmPassword}
                     />
                     <p className={styles.errors}>{errors.confirmPassword}</p>
                 </div>

                 <div className={styles.btn__login} onClick={(e) => {
                     handleSign(e)
                 }}>
                     <Button>Sign In</Button>
                 </div>
                 <p className={styles.media}>Signin with <span onClick={showMedia}>Media</span></p>
             </form>
        </div>
    )
}

export default SignIn
