import styles from './Authen.module.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faQrcode,faQuestionCircle} from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-regular-svg-icons'
import { faFacebook,faGoogle,faTwitter,faLine,faApple,faInstagram } from '@fortawesome/free-brands-svg-icons'
import LoginNotMedia from './Login'
import React from 'react'


function Login ({
    onLogout = ()=> {},
    showLogin = () => {},
    show = '',
    handleShowAuThen= () => {},
    onLogin = ()=> {},
    username='',
    onChangeUsername = ()=> {},
    password = '',
    onChangePassword = ()=> {},
    errors = '',
    modelRef = ''
}) {
    return (
          <div className={styles.login}
           ref= {modelRef}
          >
              <div className={styles.header}>
                 <div className={styles.close} onClick={handleShowAuThen}>
                 <FontAwesomeIcon icon={faTimes} />
                 </div>
              </div>
              <div className={styles.title}>
                     Đăng nhập vào TikTok
              </div>
              <div className={styles.media__login}>
                  {
                    show ?     <div className={styles.modle__container}>
                      <div className={styles.media}>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faQrcode} />
                              </div>
                              <div className={styles.name}>
                                  Sử dụng mã QR
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faUser} />
                              </div>
                              <div className={styles.name} onClick={showLogin}>
                                  Số điện thoại
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faFacebook} />
                              </div>
                              <div className={styles.name}>
                                  Đăng nhập bằng Facebook
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faGoogle} />
                              </div>
                              <div className={styles.name}>
                              Đăng nhập bằng Google
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faTwitter} />
                              </div>
                              <div className={styles.name}>
                              Đăng nhập bằng Twitter
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faLine} />
                              </div>
                              <div className={styles.name}>
                              Đăng nhập bằng LINE
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faApple} />
                              </div>
                              <div className={styles.name}>
                              Đăng nhập bằng Apple
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faInstagram} />
                              </div>
                              <div className={styles.name}>
                              Đăng nhập bằng Instagram
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faQrcode} />
                              </div>
                              <div className={styles.name}>
                                  Sử dụng mã QR
                              </div>
                          </div>
                          <div className={styles.item__media}>
                              <div className={styles.icon}>
                                  <FontAwesomeIcon icon={faQrcode} />
                              </div>
                              <div className={styles.name}>
                                  Sử dụng mã QR
                              </div>
                          </div>
                      </div>
                  </div> :  <LoginNotMedia
                    showMedia = {showLogin}
                    onLogin = {onLogin}
                    username= {username}
                    onChangeUsername = {onChangeUsername}
                    password = {password}
                    onChangePassword = {onChangePassword}
                    errors = {errors}
                   />
                  }
              </div>
   
   
              <div className={styles.footer}>
                  <div className={styles.desc}>
                       Bạn không có tài khoản ? <span onClick={onLogout}>Đăng ký</span>
                  </div>
                  <div className={styles.icon}>
                      <FontAwesomeIcon icon={faQuestionCircle} />
                  </div>          
              </div>
          </div>
    )
}

export default React.memo(Login)
