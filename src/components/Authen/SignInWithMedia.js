import styles from './Authen.module.scss'
import SignInNotMedia from  './Sign'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faQrcode,faQuestionCircle,faChevronDown} from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-regular-svg-icons'
import { faFacebook,faGoogle,faTwitter,faLine} from '@fortawesome/free-brands-svg-icons'
import { useState } from 'react'
import React from 'react'

function SignIn ({
    onSignin= ()=>{},
    showSignIn = ()=>{},
    handleSign = ()=>{},
    show = '',
    username = '',
    onChangeUsername =()=>{},
    password = '',
    onChangePassword = ()=>{},
    confirmPassword = '',
    onChangeConfirmPassword = ()=>{},
    handleShowAuThen= ()=>{},
    errors ='',
    modelRef = ''
}) {
    const [seeAll, setSeeAll] = useState(true)

    return (
        <div className={styles.login} 
         ref = {modelRef}
        >
        <div className={styles.header}>
           <div className={styles.close} onClick={handleShowAuThen}>
           <FontAwesomeIcon icon={faTimes} />
           </div>
        </div>
        <div className={styles.title}>
               Đăng Ký vào TikTok
                </div>
        <div className={[styles.media__login,styles.media__logout].join(' ')}>
          {
              show ?    <div className={styles.modle__container}>
                <div className={styles.media}>
                    <div className={styles.item__media}>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faQrcode} />
                        </div>
                        <div className={styles.name}>
                            Sử dụng mã QR
                        </div>
                    </div>
                    <div className={styles.item__media}>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faUser} />
                        </div>
                        <div className={styles.name} onClick={showSignIn}>
                            Số điện thoại
                        </div>
                    </div>
                    <div className={styles.item__media}>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faFacebook} />
                        </div>
                        <div className={styles.name}>
                            Đăng nhập bằng Facebook
                        </div>
                    </div>
                  {
                      seeAll ? <FontAwesomeIcon icon={faChevronDown} onClick={()=> {setSeeAll(false)}} /> : <div>
                      <div className={styles.item__media}>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faGoogle} />
                        </div>
                        <div className={styles.name}>
                        Đăng nhập bằng Google
                        </div>
                    </div>
                    <div className={styles.item__media}>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faTwitter} />
                        </div>
                        <div className={styles.name}>
                        Đăng nhập bằng Twitter
                        </div>
                    </div>
                    <div className={styles.item__media}>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faLine} />
                        </div>
                        <div className={styles.name}>
                        Đăng nhập bằng LINE
                        </div>
                    </div>
                      </div>
                  }
                </div>
            </div> :  <SignInNotMedia 
                    showMedia = {showSignIn}
                    handleSign = {handleSign}
                    username = {username}
                    onChangeUsername ={onChangeUsername}
                    password = {password}
                    onChangePassword = {onChangePassword}
                    confirmPassword = {confirmPassword}
                    onChangeConfirmPassword = {onChangeConfirmPassword}
                    errors ={errors}
            />
          }
        </div>
        <div className={styles.desc__logout}>
                  Bằng cách tiếp tục, bạn đồng ý với <span>Điều khoản Sử dụng</span> của TikTok và xác nhận rằng bạn đã đọc hiểu <span>Chính sách Quyền riêng tư</span> của TikTok.
                  </div>
        <div className={styles.footer}>
            <div className={styles.desc}>
                 Bạn có tài khoản ? <span onClick={onSignin}>Đăng Nhập</span>
            </div>
            <div className={styles.icon}>
                <FontAwesomeIcon icon={faQuestionCircle} />
            </div>          
        </div>
    </div>
    )
}

export default React.memo(SignIn)