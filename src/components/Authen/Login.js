import styles from './Authen.module.scss'
import Button from '~/package/tuvx-buttuon'
import React from 'react'

function Login({
    showMedia = ()=>{},
    onLogin =()=>{},
    username= '',
    onChangeUsername = ()=>{},
    password = '',
    onChangePassword = ()=>{},
    errors =''
}) {
    return (
        <div className={styles.login__not__media}>
             <form className={styles.from__login}>
                 <div className={styles.group}>
                     <label forhtml='userName' className={styles.userName}>Email :</label> <br/>
                     <input type='text' placeholder='Username' name='userName' id='userName' 
                     value={username}
                     onChange={(e)=>{
                        onChangeUsername(e)
                     }}
                     />
                     <p className={styles.errors}>{errors.email}</p>
                 </div>
                 <div className={styles.group}>
                     <label forhtml='password' className={styles.password}>Password :</label> <br/>
                     <input type='password' placeholder='Password' name='password' id='password'
                         value= {password}
                         onChange={(e)=>{
                             onChangePassword(e)
                         }}
                     />
                     <p className={styles.errors}>{errors.password}</p>
                 </div>
                 <div className={styles.btn__login} onClick={onLogin}>
                   <Button>Login</Button>
                 </div>
                 <p className={styles.media}>Login with <span onClick={()=>{
                     showMedia()
                 }}>Media</span></p>
             </form>
        </div>
    )
}

export default React.memo(Login)
