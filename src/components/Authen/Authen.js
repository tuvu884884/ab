import LoginWithMedia from './LoginWithMedia'
import SignInWithMedia from './SignInWithMedia'
import styles from './Authen.module.scss'
import React from 'react'
function Authen ({
    handle = '',
    isLogin= '',
    showContactLogin = ()=>{},
    showContactSignIn= ()=>{},
    showLogin = '',
    showSignIn = '',
    handleShowAuThen = ()=>{},
    handleLogin = ()=>{},
    handleSign =()=>{},
    username = '',
    onChangeUsername =()=>{},
    password = '',
    onChangePassword = ()=>{},
    confirmPassword = '',
    onChangeConfirmPassword = ()=>{},
    errors = '',
    modelRef = '',
}) {
    return (
       <div className={styles.authen}>
           {
               isLogin ?
               <LoginWithMedia
                   onLogout={handle}
                   showLogin = {showContactLogin}
                   show = {showLogin}
                   handleShowAuThen = {handleShowAuThen}
                   onLogin = {handleLogin}
                   username={username}
                   onChangeUsername = {onChangeUsername}
                   password = {password}
                   onChangePassword = {onChangePassword}
                   errors = {errors}
                   modelRef = {modelRef}
               /> :
               <SignInWithMedia
                    onSignin={handle}
                    showSignIn = {showContactSignIn}
                    show = {showSignIn}
                    handleShowAuThen = {handleShowAuThen}
                    handleSign = {handleSign}
                    username = {username}
                    onChangeUsername ={onChangeUsername}
                    password = {password}
                    onChangePassword = {onChangePassword}
                    confirmPassword = {confirmPassword}
                    onChangeConfirmPassword = {onChangeConfirmPassword}
                    errors = {errors}
                    modelRef = {modelRef}
               />
           }
       </div>
    )
}

export default React.memo(Authen)