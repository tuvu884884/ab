import styles from './Message.module.scss'
import { Column, Row, Grid } from "@mycv/mycv-grid";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog } from '@fortawesome/free-solid-svg-icons'
function Message() {
    return (
        <div className={styles.message}>
        <Grid type='wide' maxWidth={1142} className={styles.grid__message}>
            <Row className={styles.row__message}>
            <Column size={0} sizeTablet={5} sizeDesktop={4}>
                <div className={styles.left__part}>
                      <div className={styles.header}>
                           <h3 className={styles.title}>
                               Tin nhắn
                           </h3>
                           <div className={styles.faCog}>
                               <FontAwesomeIcon icon={faCog}/>
                           </div>
                      </div>
                      <div className={styles.emty__list}>
                          <span>Chưa có tin nhắn nào</span>
                      </div>
                </div>
            </Column>
            <Column size={12} sizeTablet={7} sizeDesktop={8}>
                <div className={styles.right__part}>
                    
                </div>
                </Column>
            </Row>
        </Grid>
        </div>
        
    )
}

export default Message
