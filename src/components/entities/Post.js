import moment from 'moment'

import BaseEntity from './BaseEntity'

class Post extends BaseEntity {
    static type = 'post'
    static isPlay = true
    get video_width() {
        return this.meta.video.resolution_x
    }
    
    get video_height() {
        return this.meta.video.resolution_y
    }

    get is_video_horizontal() {
        return this.video_width > this.video_height
    }

    get video_ratio() {
        return this.video_width / this.video_height
    }

    get computed_video_width() {
        if (this.is_video_horizontal) {
            return `calc((400px + ((100vw - 768px) / 1152) * 100))`
        }
        return `calc(${this.video_ratio} * (400px + ((100vw - 768px) / 1152) * 100))`
    }
    get computed_video_hight () {
        if(this.is_video_horizontal) {
            return this.video_ratio * this.computed_video_width
        }
        else {
            return this.computed_video_width / this.video_ratio
        }
    }
    
    set playing(isPlay = true) {
        return this.isPlay = isPlay
    }
    get playing() {
        return this.isPlay
    }
    get computed_top_offset() {
        // 1/3 viewport (- Header 60px)
        return (window.innerHeight + 60) * 0.05
    }

    get computed_bottom_offset() {
        return (window.innerHeight - 60) * 0.2
    }
    

    get published_at_from_now() {
        return moment(this.published_at).fromNow()
    }
}
BaseEntity.addSubClass(Post)
Object.defineProperty(BaseEntity.subClasses, Post.type, {
    value: Post,
})

export default Post
