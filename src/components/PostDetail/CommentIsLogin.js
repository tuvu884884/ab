import styles from './PostDetail.module.scss'
import { Waypoint } from 'react-waypoint'
import { Fragment } from 'react'
import config from '~/config'
import {
    Link
} from 'react-router-dom'
function CommentIsLogin({
    comments = '',
    comment = '',
    onChangeText = () => { },
    onPostComment = () => { },
    onSeeComment = () => { },
}) {
    let Component = Fragment
    const props = {}
    return (
        <>
            <div className={styles.comment__container}>
                <div className={styles.comments}>
                    {
                        comments.map((comment, i) => {
                            if (comments.length - 1 === i) {
                                Component = Waypoint
                                props.onEnter = onSeeComment
                            }
                            return (
                                <Component {...props} key={i}>
                                    <div className={styles.comment__item}>
                                        <Link to={`${config.routes.home}profile/@${comment.user.nickname}`}>
                                            <div className={styles.avatar}>
                                                <img src={comment.user.avatar} alt='' />
                                            </div>
                                        </Link>
                                        <div className={styles.comment__user}>
                                            <Link to={`${config.routes.home}profile/@${comment.user.nickname}`}>
                                                <div className={styles.user__name}>
                                                    {
                                                        comment.user.nickname
                                                    }
                                                </div>
                                            </Link>
                                            <div className={styles.comment__text}>
                                                {
                                                    comment.comment
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </Component>
                            )
                        })
                    }
                </div>
            </div>
            <div className={styles.comment__post}>
                <div className={styles.text}>
                    <input placeholder='Thêm bình luận' className={styles.content__text}
                        onChange={onChangeText}
                        value={comment}
                    />
                </div>
                <button type='submit' onClick={() => onPostComment()}>Đăng</button>
            </div>
        </>
    )
}

export default CommentIsLogin
