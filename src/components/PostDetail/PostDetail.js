import styles from './PostDetail.module.scss'
import ReactPlayer from 'react-player'
import Button from '../../package/tuvx-buttuon'
import ComponentNotLogin from './CommentNotLogin'
import ComponentIsLogin from './CommentIsLogin'
import {useSelector,useDispatch} from 'react-redux'
import { actions as actionsModel} from '~/state/sateModel'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faVolumeDown,faVolumeMute,faPlay, faFlag, faHeart, faComment, faLink, faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { faWhatsapp, faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons'
import config from '~/config'
import {
    Link
} from 'react-router-dom'

function PostDetial({
    post = '',
    avatar = '',
    name = '',
    nickname = '',
    description = '',
    url__video = '',
    likeCount = '',
    sharesCount = '',
    commentsCount = [],
    hidePostDetails = () => { },
    continueVideo = () => { },
    currentTime = '',
    isFirst = '',
    isLast = '',
    comments = '',
    comment = '',
    isPlay= '',
    closeRef = '',
    nextRef = '',
    prveRef = '',
    valumeRef = '',
    videoRef = '',
    muted = '',
    type = '',
    onToggleFollow=()=>{},
    setMuted=()=>{},
    onChangePlay= () => {},
    handleNextPost = () => { },
    handlePrevPost = () => { },
    onToggleLiked = () => { },
    handleChangText = () => { },
    handlePostComment = () => { },
    handleSeeComment = () => { },
    copyClipBoard = () => { },
}) {
    const data = useSelector(state => state.user.currentUser)
    const stateModal = useSelector(state=> state.stateModel.stateModal)
    const dispatch = useDispatch()
    const handleShowModal = ()=> {
        hidePostDetails(post.id)
        if(stateModal) {
            dispatch(actionsModel.handleHideModal())
        } else {
            dispatch(actionsModel.handleShowModal())
        }
    }
    return (
        <div className={styles.postDetail}>
            <div className={styles.video} onClick={onChangePlay} ref={videoRef}>
              {
                  !isPlay&&
                  <div className={styles.faPlay} onClick={onChangePlay}>
                  <FontAwesomeIcon icon={faPlay}/>
                  </div>
              }
                {
                    !isFirst && <div className={styles.faChevronLeft} 
                    onClick={handlePrevPost}
                    ref={prveRef}
                    >
                        <FontAwesomeIcon icon={faChevronLeft}
                         style={
                            {
                                position: 'relative',
                                left: '-1px',
                            }
                        } />
                    </div>
                }
                {
                    !isLast && <div className={styles.faChevronRight} 
                    onClick={handleNextPost}
                    ref={nextRef}
                    >
                        <FontAwesomeIcon icon={faChevronRight} 
                        style={
                            {
                                position: 'relative',
                                left: '-1px'
                            }
                        } />
                    </div>
                }
                <div className={styles.overlay}>
                </div>
                    <ReactPlayer
                        width='995px'
                        height='100vh'
                        url={url__video}
                        playing={isPlay}
                        muted = {muted}
                        loop
                        ref={(ref) => {
                            continueVideo(ref, currentTime, post.id)
                        }}
                    />
                <div className={[styles.close, styles.icon].join(' ')} ref={closeRef}
                    onClick={()=>hidePostDetails(post.id)}>
                    <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                </div>
                <div className={[styles.faVolumeDown, styles.icon].join(' ')} onClick={() =>setMuted(!muted)}
                ref= {valumeRef}
                >
                    <FontAwesomeIcon icon={!muted?faVolumeDown:faVolumeMute}></FontAwesomeIcon>
                </div>
                <div className={styles.report}>
                    <FontAwesomeIcon icon={faFlag} className={styles.faFlag}></FontAwesomeIcon>
                    <span>Báo Cáo</span>
                </div>
            </div>

            <div className={styles.content}>
                <div className={styles.user__info}>
                    <div className={styles.user}>
                    <Link to={`${config.routes.home}profile/@${nickname}`}>
                    <img className={styles.user__img} src={avatar} alt="" />
                    </Link>
                        <div className={styles.user__info__link}>
                            <Link to={`${config.routes.home}profile/@${nickname}`}>
                            <div className={styles.name}>{name}</div>
                    </Link>
                            <div className={styles.nickname}>{nickname}</div>
                        </div>
                        {   
                            !(type === "following")&&(
                            !post.user.is_followed  ? <Button type='border' onClick={()=> data? onToggleFollow(post,post.id):handleShowModal()}>Follow</Button> : 
                            <Button type='normal' following='following' onClick={()=> onToggleFollow(post,post.id)}>Đang Following</Button>
                            )
                        }
                    </div>
                    <div className={styles.description}>
                        {description}
                    </div>
                    <div className={styles.action}>
                        <div className={styles.action__left}>
                            <div className={styles.group}>
                                <div className={post.is_liked ? [styles.icon, styles.faHeart, styles.active].join(' ') : [styles.icon, styles.faHeart].join(' ')}
                                    onClick={() => onToggleLiked(post)}
                                >
                                    <FontAwesomeIcon icon={faHeart}></FontAwesomeIcon>
                                </div>
                                <span>{likeCount}</span>
                            </div>
                            <div className={styles.group}>
                                <div className={[styles.icon, styles.faHeart].join(' ')}>
                                    <FontAwesomeIcon icon={faComment}></FontAwesomeIcon>
                                </div>
                                <span>{commentsCount}</span>
                            </div>
                        </div>

                        <div className={styles.action__right}>
                            <span>Chia sẻ với</span>
                            <FontAwesomeIcon icon={faWhatsapp} className={styles.icon}></FontAwesomeIcon>
                            <FontAwesomeIcon icon={faFacebook} className={styles.icon}></FontAwesomeIcon>
                            <FontAwesomeIcon icon={faTwitter} className={styles.icon}></FontAwesomeIcon>
                            <FontAwesomeIcon icon={faLink} className={styles.icon}></FontAwesomeIcon>
                        </div>
                    </div>
                    <div className={styles.copy__link}>
                        <div className={styles.link}>
                            https://www.tiktok.com/@hoathinh_6891/video/6942512793975377153?is_copy_url=0&is_from_webapp=v1&sender_device=pc&sender_web_id=6877115452973385218
                              </div>
                        <div className={styles.link__icon} onClick={()=>copyClipBoard(nickname)}>
                            <FontAwesomeIcon icon={faLink} className={styles.icon}></FontAwesomeIcon>
                            <span>Sao Chép Liên Kết</span>
                        </div>
                    </div>
                    <div className={styles.line}></div>
                </div>
                    {
                            data ? <ComponentIsLogin
                                comments={comments}
                                comment={comment}
                                onChangeText={handleChangText}
                                onPostComment={handlePostComment}
                                onSeeComment={handleSeeComment}
                            /> : <ComponentNotLogin 
                            hidePostDetails={hidePostDetails}
                            id={post.id}
                            />
                    }
            </div>
        </div>
    )
}

export default PostDetial
