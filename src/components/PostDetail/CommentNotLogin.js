import styles from './PostDetail.module.scss'
import Button from '../../package/tuvx-buttuon'
import {useSelector, useDispatch} from 'react-redux'
import { actions as actionsModel} from '~/state/sateModel'

function CommentNotLogin({
    hidePostDetails=()=>{},
    id='',
}) {
    const stateModal = useSelector(state=> state.stateModel.stateModal)
    const dispatch = useDispatch()
    const handleShowModal = ()=> {
        hidePostDetails(id)
        if(stateModal) {
            dispatch (actionsModel.handleHideModal())
        } else {
            dispatch (actionsModel.handleShowModal())
        }
    }
    return (
        <div className={styles.comment}>
                        <div className={styles.logout}>
                             <h3>Đăng nhập để xem bình luận</h3>
                             <p className={styles.desc}>Đăng nhập để xem bình luận và thích video.</p>
                             <Button size='l' onClick={handleShowModal}>Đăng Nhập</Button>
                             <p className={styles.sign__up}>Bạn không có tài khoản? <span onClick={handleShowModal}>Đăng Ký</span></p>
                        </div>
        </div> 
    )
}

export default CommentNotLogin
