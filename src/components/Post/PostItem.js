import styles from './Post.module.scss'
import ReactPlayer from 'react-player'
import Button from '../../package/tuvx-buttuon'
import config from '~/config'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faVolumeDown, faVolumeMute, faPlay, faPause, faHeart, faShare} from '@fortawesome/free-solid-svg-icons'
import { faFlag, faComment } from '@fortawesome/free-regular-svg-icons'
import { useRef, useState } from 'react'
import { Waypoint } from 'react-waypoint'
import React from 'react';
import Tippy from '@tippyjs/react/headless'
import TippyShare from './TipyShare'
import { Link } from 'react-router-dom'


function PostItem ({
    playingFirst = '',
    datas= '',
    setPosts= ()=> {},
    data = '',
    id = '',
    name='',
    nickname = '',
    avatar = '',
    urlVideo ='',
    desc= '',
    likeCount = '',
    commentCount = '',
    shareCount ='',
    playing= '',
    type= '',
    setPlaying= ()=> {},
    getVideoRef =()=>{},
    handleShowDetailPost = ()=>{},
    onToggleLiked = ()=>{},
    isPlay = '',
    handlePlayVideo=()=>{},
    setPlayingFrist=()=>{},
    onToggleFollow = ()=>{},
    copyClipBoard = () => { },
}) {
    const [volumeDown,setVolumeDown] = useState(true)
    const videoRef = useRef()
    function changeVolume() {
        setVolumeDown(!volumeDown)
    }
    function onPause () {
        setPlaying(false)
    }
    const hanleEnterViewport = ()=> {
        setPlayingFrist(false)
        window.onscroll = ()=> {
            let rect
            let top
            if(videoRef.current)
           {
             rect = videoRef.current.getInternalPlayer().getBoundingClientRect()
             top = Math.trunc(rect.top)

           }
            if((top >=200 || top <=650) || (top >=-300 && top <=100)) {
                const index = datas.lastIndexOf(data)
                if(top >= 150) {
                    if(!(index === 0))
                    handlePlayVideo(datas[index-1].id)
                }else if(top <=-150){
                    if(!(index === datas.length - 1))
                handlePlayVideo(datas[index+1].id)
               } else {

                handlePlayVideo(id)
               }
            }
        }
        
    }
    const Ref = (ref)=> {
        videoRef.current = ref
    }
    
    return(
        <div className={styles.postItem}>
        <Link to={`${config.routes.home}profile/@${nickname}`}>
        <div className={styles.avatar}>
             <img src={avatar} alt=""/>
         </div>
        </Link>
         <div className={styles.info}>
             <div className={styles.authorInfo}>
                 <Link to={`${config.routes.home}profile/@${nickname}`}>
                 <h3 className={styles.name}>{name}</h3>
                 </Link>
                 <span className={styles.nickname}>{nickname}</span>
             </div>
             <div className={styles.captions}>
                 <span>{desc}</span>
             </div>
             <div className={styles.video} style= {{
                   width: (data.computed_video_width),
                   height: (data.computed_video_hight)
             }}>     
                    
                        <div className={styles.video__info}>
                        <Waypoint
                        onEnter={()=>{
                            hanleEnterViewport(videoRef)
                            }}
                        onLeave={()=>{
                            hanleEnterViewport(videoRef)
                        }}
                        //  topOffset ={data.computed_top_offset}
                        //  bottomOffset ={data.computed_bottom_offset}
                        >
                            <div className={styles.waypoint}></div>
                        </Waypoint>
                       <ReactPlayer
                            url={urlVideo}
                            height = {data.computed_video_hight}
                            width= {data.computed_video_width}
                            playing={(isPlay && playing)||playingFirst}
                            muted = {volumeDown}
                            loop
                            onClick={()=> {
                                handleShowDetailPost(data)
                            }}
                            ref = { (ref)=> {
                                getVideoRef(ref,id)
                                Ref(ref)
                            }}
                        >
                        </ReactPlayer>
                        </div>
                    
                                {!volumeDown ? 
                                <FontAwesomeIcon icon={faVolumeDown} className={styles.faVolume} onClick={changeVolume}/>:
                                <FontAwesomeIcon icon={faVolumeMute} className={styles.faVolume} onClick={changeVolume}/>        
                                }
                    <div className={styles.report}>
                            <div className={styles.icon}>
                                <FontAwesomeIcon icon={faFlag}/>
                            </div>
                            <div className={styles.text}>
                                Báo Cáo
                            </div>
                    </div>
                               
                                { 
                                    (isPlay && playing)||playingFirst ?
                                <FontAwesomeIcon icon={faPause} className={styles.faPlayPasue} onClick={onPause}/>:
                                <FontAwesomeIcon icon={faPlay} className={styles.faPlayPasue} onClick={()=> {
                                    // onChangePlay()
                                    handlePlayVideo(id)
                                    }}/>        
                                }
                        <div className={styles.media}>
                            <div className={styles.group}>
                                <div className={data.is_liked ? [styles.icon,styles.active].join(' '):styles.icon} onClick={()=>onToggleLiked(data)} >
                                   <FontAwesomeIcon icon={faHeart}/>
                                </div>
                                <div className={styles.total}>
                                        {likeCount}
                                </div>
                            </div>
                            <div className={styles.group}>
                                <div className={styles.icon} onClick={()=>handleShowDetailPost(data)}>
                                   <FontAwesomeIcon icon={faComment}/>
                                </div>
                                <div className={styles.total}>
                                        {commentCount}
                                </div>
                            </div>
                            <Tippy
                            render = {()=>(
                                  <TippyShare
                                      data={data}
                                      copyClipBoard = {copyClipBoard}
                                  />
                            )}
                            interactive={true}
                            >
                                    <div className={styles.group}>
                                        <div className={styles.icon}>
                                        <FontAwesomeIcon icon={faShare}/>
                                        </div>
                                        <div className={styles.total}>
                                                {shareCount}
                                        </div>
                                    </div>
                            </Tippy>
                        </div>
                     </div>
         </div>


       <div className={styles.following}>
       {   
            !(type === "following")&&(
            !data.user.is_followed  ? <Button type='border' onClick={()=> onToggleFollow(data,id)}>Follow</Button> : 
            <Button type='normal' following='following' onClick={()=> onToggleFollow(data,id)}>Đang Following</Button>
            )
        }
       </div>
        </div>
    )
}

export default PostItem