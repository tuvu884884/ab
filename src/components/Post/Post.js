import styles from './Post.module.scss'
import PostList from './PostList'

function Post ({
    posts = [],
    type= '',
    setPosts = () => {},
    handleVideoRef = ()=>{},
    isScrollDown = '',
    videoRefs = '',
    handleShowDetailPost = ()=>{},
    playingVideoClose = '',
    handleToggleLiked= ()=>{},
    handleToggleFollow = ()=>{},
    copyClipBoard = () => { },
}) {
   return (
       <div className={styles.post}>
           <PostList 
                posts = {posts}
                type= {type}
                setPosts = {setPosts}
                handleVideoRef = {handleVideoRef}
                isScrollDown = {isScrollDown}
                videoRefs = {videoRefs}
                handleShowDetailPost = {handleShowDetailPost}
                playingVideoClose = {playingVideoClose}
                onToggleLiked = {handleToggleLiked}
                onToggleFollow = {handleToggleFollow}
                copyClipBoard = {copyClipBoard}
           />
       </div>
   )
}

export default Post
