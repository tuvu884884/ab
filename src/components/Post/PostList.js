import styles from './Post.module.scss'
import PostItem from './PostItem'
import {useEffect, useRef, useState} from 'react'


function PostList ({
    posts = [],
    setPosts= ()=> {},
    handleVideoRef = () => {},
    isScrollDown = '',
    videoRefs = '',
    handleShowDetailPost = () => {},
    playingVideoClose = '',
    onToggleLiked = ()=> {},
    type='',
    onToggleFollow = ()=> {},
    copyClipBoard = () => { },
}) {
   const [id,setID] = useState()
   const listPlayRef = useRef({})
   const [playingFirst,setPlayingFrist] = useState(true)
   const [playing, setPlaying] = useState(true)

  useEffect(() => {
    if(posts[0]) {
        listPlayRef.current[posts[0].id] = true
    }
  },[posts])
  const handlePlayVideo = (id)=> {
    setPlaying(true)
      setID(()=>{
        posts.forEach(post => {
            if(post.id === id){
                listPlayRef.current[post.id] = true
            } else {
               listPlayRef.current[post.id] = false
            }
          })
          return id
      })
  }
   return (
       <div className={styles.postList}>
      {
          posts.map((post,index) => {
              return (
               <PostItem 
                    isFrist = {index === 0}
                    datas={posts}
                    setPosts= {setPosts}
                    data = {post}
                    key= {post.id}
                    id = {post.id}
                    name={post.user.last_name + ' ' + post.user.first_name}
                    nickname = {post.user.nickname}
                    avatar = {post.user.avatar}
                    urlVideo = {post.file_url}
                    desc = {post.description}
                    widthVideo = {post.meta.video.resolution_x}
                    heightVideo = {post.meta.video.resolution_y}
                    likeCount = {post.likes_count}
                    commentCount = {post.comments_count}
                    shareCount = {post.shares_count}
                    getVideoRef = {handleVideoRef}
                    handleShowDetailPost = {handleShowDetailPost}
                    onToggleLiked = {onToggleLiked}
                    isPlay={listPlayRef.current[post.id]}
                    handlePlayVideo={handlePlayVideo}
                    playingFirst={playingFirst&&index===0}
                    setPlayingFrist= {setPlayingFrist}
                    playing = {playing}
                    setPlaying= {setPlaying}
                    type={type}
                    onToggleFollow = {onToggleFollow}
                    copyClipBoard={copyClipBoard}
                />
              )
          })
      }
       </div>
   )
}

export default PostList
