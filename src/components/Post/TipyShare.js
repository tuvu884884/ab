import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook,faWhatsappSquare,faTwitter } from '@fortawesome/free-brands-svg-icons'

import styles from './Post.module.scss'
import config from '~/config'
import { faLink } from '@fortawesome/free-solid-svg-icons'

function TipyShare({
    data='',
    copyClipBoard = () => { },
}) {
    return (
        <div className={styles.tipyShare}>
             <div className={styles.group__share}>
                 <div className={styles.icon}>
                      <FontAwesomeIcon icon={faFacebook}/>
                 </div>
                 <div className={styles.text} onClick={()=>{
                     window.open(config.socials.shares.facebook(data.uuid))
                 }}>
                     Chia sẻ với Facebook
                 </div>
             </div>
             <div className={styles.group__share}>
                 <div className={styles.icon}>
                      <FontAwesomeIcon icon={faWhatsappSquare}/>
                 </div>
                 <div className={styles.text} onClick={()=>{
                     window.open(config.socials.shares.whatsapp(data.uuid))
                 }}>
                     Chia sẻ với Whatsapp
                 </div>
             </div>
             <div className={styles.group__share}>
                 <div className={styles.icon}>
                      <FontAwesomeIcon icon={faTwitter}/>
                 </div>
                 <div className={styles.text} onClick={()=>{
                     window.open(config.socials.shares.twitter(data.uuid))
                 }}>
                     Chia sẻ với Twitter
                 </div>
             </div>
             <div className={styles.group__share}>
                 <div className={styles.icon}>
                 <FontAwesomeIcon icon={faLink}></FontAwesomeIcon>
                 </div>
                 <div className={styles.text} onClick={()=>{
                    copyClipBoard(data.user.nickname)
                 }}>
                     Copy Link
                 </div>
             </div>
        </div>
    )
}

export default TipyShare
