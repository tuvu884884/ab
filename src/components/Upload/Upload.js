import styles from './Upload.module.scss'
import { Column, Row } from "@mycv/mycv-grid";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCloudDownloadAlt } from '@fortawesome/free-solid-svg-icons'
function Upload() {
    return (
        <Row className={styles.upload}>
            <div className={styles.title}>
                <span>Tải video lên</span>
                <p>Video này sẽ được công bố cho @user533501692</p>
            </div>
            <Column size={0} sizeTablet={5} sizeDesktop={3}>
              <label for="upload__info">
              <div className={styles.upload__btn}>
                  <div className={styles.background}>
                        <div className={styles.faCloudDownloadAlt}>
                            <FontAwesomeIcon icon={faCloudDownloadAlt}/>
                        </div>
                        <div className={styles.text__main}>
                            <span>Chọn video để tải lên</span>
                        </div>
                        <div className={styles.text__sub}>
                           <span> Hoặc kéo và thả tập tin</span>
                        </div>
                        <ul>
                            <li>MP4 hoặc WebM</li>
                            <li>Độ phân giải 720x1280 trở lên</li>
                            <li>Tối đa 180 giây</li>
                        </ul>
                  </div>
              </div>
              </label>
              <input type="file" hidden id="upload__info"></input>
            </Column>
            <Column size={12} sizeTablet={7} sizeDesktop={9}>
                <div className={styles.from}>
                    <div className={styles.note}>
                            <div className={styles.font}>
                                <div className={styles.title__font}>
                                Chú thích
                                </div>
                                <div className={styles.require__font}>
                                    0/150
                                </div>
                            </div>
                    </div>
                    <div className={styles.text__box}>
                        <div className={styles.editor}>
                            <div className={styles.text__editor} aria-autocomplete="list" contenteditable="true" maxlength='150'>
                                 
                            </div>
                        </div>
                    </div>
                    <div className={styles.font__item}>
                        <div className={styles.title}>
                            Ảnh bìa
                        </div>
                        <div className={styles.empty}>
                            <div className={styles.candidate}>

                            </div>
                        </div>
         
                    </div>
                    <div className={styles.font__item__flex}>
                        <div className={styles.permission}>
                            <div className={styles.title}>
                            Ai có thể xem video này
                            </div>
                            <div className={styles.radio__group}>
                                <label className={styles.radio}>
                                    <input type="radio" value checked/>
                                    <span className={styles.radio__box}></span>
                                    <p>Công khai</p>
                                </label>
                                <label className={styles.radio}>
                                    <input type="radio" value />
                                    <span className={styles.radio__box}></span>
                                    <p>Bạn bè</p>
                                </label>
                                <label className={styles.radio}>
                                    <input type="radio" value />
                                    <span className={styles.radio__box}></span>
                                    <p>Riêng Tư</p>
                                </label>
                            </div>
                        </div>
                        <div className={styles.permission}>
                            <div className={styles.title}>
                            Cho phép người dùng:
                            </div>
                            <div className={styles.radio__group}>
                                <label className={styles.radio}>
                                    <input type="checkbox" value/>
                                    <p>Bình luận</p>
                                </label>
                                <label className={styles.radio}>
                                <input type="checkbox" value/>
                                    <p>Duet / React</p>
                                </label>
                                <label className={styles.radio}>
                                <input type="checkbox" value/>
                                    <p>Stitch</p>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className={styles.op__part}>
                        <button type="button" className={styles.btn__cancle}>Hủy bỏ</button>
                        <button type="button" className={styles.btn__post} disabled>Đăng</button>

                    </div>
                </div>
            </Column>
        </Row>
    )
}

export default Upload
