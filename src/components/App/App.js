import { Grid } from '@mycv/mycv-grid'
import { useState } from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import FooterComponent from '~/components/Footer'
import MessageComponent from '~/components/Message'
import SearchContainer from '~/containers/Search'
import UploadContainer from '~/containers/Upload'
import config from '../../config'
import FollowingContainer from '../../containers/Following'
import HerderContainer from '../../containers/Herder'
import HomeContainer from '../../containers/Home'
import PostDetialContainer from '../../containers/PostDetail'
import ProFileContainer from '../../containers/Profile'
import styles from './App.module.scss'
import AuthenContainer from '~/containers/Authen'
import NotFoundContainer from '~/containers/NotFound'
import {Provider} from '~/state/user'
import {useSelector} from 'react-redux'

function App () {
      const renderWithSidebar = Component  => {
        return () => (
            <Grid type='wide' maxWidth={1142}>
              <Component/>
             </Grid>
        )
    }
   const stateModal = useSelector(state=> state.stateModel.stateModal)
   console.log(stateModal)
    return (
           <div>
            {
              stateModal && <AuthenContainer/>
            }
              <Provider/>
                <Router basename={config.routes.base}>
                                <div className= {styles.herder__container} >
                                    <HerderContainer />
                                </div>
                                <Switch>   
                                        <Route exact path={config.routes.home} render={renderWithSidebar(HomeContainer)}/>
                                        <Route exact path={config.routes.following} render={renderWithSidebar(FollowingContainer)}/>
                                        <Route exact path={config.routes.profile} render={renderWithSidebar(ProFileContainer)}/>
                                        <Route exact path={config.routes.search} render={renderWithSidebar(SearchContainer)}/>
                                        <Route exact path={config.routes.postDetail} render={renderWithSidebar(PostDetialContainer)}/>
                                        <Route exact path={config.routes.upload} render={renderWithSidebar(UploadContainer)}/>
                                        <Route exact path={config.routes.upload} render={renderWithSidebar(FooterComponent)}/>
                                        <Route exact path={config.routes.message} render={renderWithSidebar(MessageComponent)}/>
                                        <Route component={NotFoundContainer}/>
                                </Switch>
                </Router>
                </div>
      )
}

export default App
