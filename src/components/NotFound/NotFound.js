import React from 'react'
import styles from './NotFound.module.scss'
import iconError from '~/accssets/img/icon-error.png'
import Button from '~/package/tuvx-buttuon'
import Footer from '~/components/Footer'
function NotFound({
    handlefromHome = ()=>{}
}) {
    return (
       <>
        <div className={styles.notFound}>
           <div className={styles.error}>
               <h1>
               <span>4</span>
               <img src={iconError} alt=""/>
               <span>4</span>
               </h1>
           </div>
           <div className={styles.notFound__desc}>Couldn't find this page</div>
           <div className={styles.recommend__desc}>Xem những video thịnh hành khác trên TikTok</div>
           <div className={styles.btn__link} onClick={handlefromHome}>
           <Button size='l' className={styles.btn__link}>Xem ngay</Button>
           </div>
        </div>
        <Footer/>
       </>
    )
}

export default NotFound
