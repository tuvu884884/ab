import styles from './Navbar.module.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons'

function Mid ({
    heading = '',
    see = '',
    onSeeToggle = ()=> {},
    seeAll = true,
    hideSeeBtn = true,
    children = null
}) {
    return (
       <div>
           <h3 className={styles.title}>
               {heading}
           </h3>
           {
          children
           }
           {
            hideSeeBtn ? <div className={styles.see} onClick ={onSeeToggle}>
               <p className={styles.text}>{see}</p>
               <FontAwesomeIcon icon={!seeAll ? faChevronUp : faChevronDown} className={styles.faChevron}/>
           </div> : null
           }
       </div>
    )
}

export default Mid
