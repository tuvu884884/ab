
import styles from './Navbar.module.scss'
import { Link } from 'react-router-dom'
import config from '../../config'
import AccountPrivew from '../AccountPrivew'

import { Fragment } from 'react'
import { Waypoint } from 'react-waypoint'
import Tippy from '@tippyjs/react/headless'
import 'tippy.js/dist/tippy.css'

function MidItem({
    data = '',
    name = '',
    nickname = '',
    avatar = '',
    children,
    isLast = false,
    isFollowed='',
    followersCount='',
    likesCount='',
    tick = true,
    onEnter = () => {},
    apiPath = '',
    handleFollow = () => {},
}) {
    let Component = Fragment
    const props = {}
    if(isLast) {
        Component = Waypoint
        props.onEnter = onEnter
    }
    return (
      <Component {...props}>
          <div>
          <Tippy
        interactive
        delay={500}
        offset={[20,0]}
        placement="bottom"
        appendTo={() => document.body}
         render={() => (
            <AccountPrivew 
            avatar={avatar}
            nickname={nickname}
            tick={tick}
            fullName={name}
            followersCount={followersCount}
            isFollowed = {isFollowed}
            likesCount={likesCount}
            apiPath= {apiPath}
            onFollow= {handleFollow}
            data = {data}
            />
    )}>
            <Link to={`${config.routes.home}profile/@${nickname}`}>
                <div className={styles.group__info}>
                    <div className={styles.avatar}>
                        <img src={avatar} alt='' />
                    </div>
                    <div className={styles.info}>
                        <h4>{name}</h4>
                        <p>{nickname}</p>
                    </div>
                </div>
                {children}
            </Link>
        </Tippy>
          </div>
      </Component>
    )
}

export default MidItem
