import {
    NavLink
  } from 'react-router-dom'

import styles from './Navbar.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faUserFriends } from '@fortawesome/free-solid-svg-icons'
import Button from '../../package/tuvx-buttuon'
import config from '../../config'
import {useSelector} from 'react-redux'
function Top ({
    showModal= ()=>{}
}) {
    const data = useSelector(state => state.user.currentUser)
    return (
         <div className={styles.topNavbar}>
             <NavLink  exact to={config.routes.home}
              activeStyle={{
                    fontWeight: "bold",
                    color: 'rgb(254, 44, 85)',
                }}
             >
             <div className={styles.group}>
                <FontAwesomeIcon icon={faHome} className={styles.icon}/>
                <div className={styles.text}>
                    Dành cho bạn
                </div>
             </div>
             </NavLink>
            <NavLink exact to={config.routes.following}
              activeStyle={{
                    fontWeight: "bold",
                    color: 'rgb(254, 44, 85)',
                }}
            >
            <div className={[styles.group,styles.normal].join(' ')}>
                <FontAwesomeIcon icon={faUserFriends} className={styles.icon}/>
                <div className={styles.text}>
                    Đang Follow
                </div>
             </div>
            </NavLink>
                      {
                              data ? null :
                              <div className={styles.upper__contents}>
                                    <span>Đăng nhập để follow các tác giả, thích video và xem bình luận.</span>
                                    <div className={styles.btn} onClick={showModal}>
                                       <Button type="border" size="l">Đăng Nhập</Button>
                                    </div>
                                </div>
                      }
         </div>
    )
}

export default Top
