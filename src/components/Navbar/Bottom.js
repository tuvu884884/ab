import styles from './Navbar.module.scss'
import 'tippy.js/dist/tippy.css' 
import 'tippy.js/themes/light.css' 

import Tippy from '@tippyjs/react'


function Bottom () {
    return (
        <div className={styles.bottomNavbar}>
            <div className={styles.contact}>
                 <span>Giới thiệu</span>
                 <span>Bảng tin</span>
                 <span>Liên hệ</span>
                 <span>Sự nghiệp</span>
                 <span>ByteDance</span>
            </div>
            <div className={styles.contact}>
                 <span>TikTok for Good</span>
                 <span>Advertise</span>
                 <span>Developers</span>
                 <span>Transparency</span>
            </div>
            <div className={styles.contact}>
                 <span>Trợ giúp</span>
                 <span>An toàn</span>
                 <span>Điều khoản</span>
                 <span>Quyền riêng tư</span>
                 <span>Creator Portal</span>
                 <span>Hướng dẫn Cộng đồng</span>
            </div>
            <div className={styles.more}>
           <Tippy
                content = {
                     <div className={styles.waring}>Tuân thủ pháp luật</div>
                }
                theme={'light'}
                interactive={true}
                delay={500}
                offset={[0,0]}
                disabled = {false}
                >
                <span>More</span>
            </Tippy></div>
           <span className={styles.coyyRight}>© 2021 TikTok</span>
        </div>
    )
}

export default Bottom
