import styles from './Navbar.module.scss'


function Navbar ({children = null}) {
   return(
           <div className={styles.navbar}>
           {children}
       </div>
   )
}
         
export default Navbar
