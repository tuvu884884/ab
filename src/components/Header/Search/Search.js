import styles from './Search.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import SearchView from './SearchView'

import 'tippy.js/dist/tippy.css'

function Search ({
    usersSearch = [],
    onchangeValuSearch = ()=>{},
    valuSearch = '',
    closeSearch=()=>{},
    isusersSearch = '',
    handleViewAllSearchResult = ()=>{},
    searchViewRef ='',
}) {
   return (
        <div className={styles.search__container}>
            <div className={styles.search__input}>
             <div className={styles.iconEllipsisV}>
                 <input type="text" placeholder='Tìm kiếm tài khoản' value={valuSearch} onChange={onchangeValuSearch}/>
                </div>
                <span className={styles.split}></span>
                <button className={styles.button__search__container} onClick={handleViewAllSearchResult}>
                <FontAwesomeIcon className={styles.faSearch} icon={faSearch}/>
                </button>
            </div>
            {
                valuSearch ?  <div className={styles.faTimesCircle}>
            <FontAwesomeIcon icon={faTimesCircle} onClick={closeSearch}/>
                 </div> : null
            }
          {
              (isusersSearch && usersSearch) ?  
              <SearchView
               usersSearch = {usersSearch}
               searchViewRef = {searchViewRef}
           /> : null
          }
        </div>
   )
}

export default Search
