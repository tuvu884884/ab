import styles from './Search.module.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import {
    Link
} from 'react-router-dom'
import config from '~/config'

function SearchView({
    usersSearch = [],
    searchViewRef =''
}) {
    return (
        <div className={styles.SearchViewName}
        ref={searchViewRef}
        >
            {
                usersSearch.map((user) => {
                    return (
                        <Link to={`${config.routes.home}profile/@${user.nickname}`}>
                        <div className={styles.info}
                         key= {user.id}
                        >
                            <div className={styles.info__name}>
                                <p className={styles.name}>
                                    {user.first_name + '' + user.last_name}
                                    </p>
                                {
                                   user.tick ? <FontAwesomeIcon className={styles.faCheckCircle} icon={faCheckCircle} /> : null
                                }
                            </div>
                            <div className={styles.info__nickname}>
                                <p className={styles.nickname}> {user.nickname}</p>
                            </div>
                        </div>
                        </Link>
                    )
                })
            }
        </div>
    )
}

export default SearchView
