import {
   Link, useHistory
 } from 'react-router-dom'

import logoDark from '../../../accssets/img/logo-dark.svg'
import logoTextDark from '../../../accssets/img/logo-text-dark.svg'
import styles from './Logo.module.scss'
import config from '../../../config'


function Logo () {
    const history = useHistory()
    return (
        
        <Link to={config.routes.base+config.routes.home} onClick={()=> {
            history.push(`${config.routes.home}`)
            window.location.reload()
        }}>
        <div className={styles.logo__link}>
           <div className={styles.logo__dark}>
                <img src={logoDark} alt=''/>
            </div>
            <div className={styles.logo__text__dark}>
                <img src={logoTextDark} alt=''/>
            </div>
        </div>
        </Link>
    )
}

export default Logo
