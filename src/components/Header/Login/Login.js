import IsLogin from './IsLogin'
import NotLogin from './NotLogin'
import {useSelector} from 'react-redux'

function Login ({
    handleShowAuThen = ()=> {},
    hangleLogOut = ()=> {},
}) {
    const data = useSelector(state => state.user.currentUser)
    return (
                    data ? <NotLogin
                        avatar={data.avatar}
                        nickname = {data.nickname}
                        onLogOut ={hangleLogOut}
                    /> : <IsLogin
                    onShowAuthen = {handleShowAuThen}
                    />
    )
}

export default Login
