import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons'
import 'tippy.js/dist/tippy.css' 
import Tippy from '@tippyjs/react/headless'

import Button from '../../../package/tuvx-buttuon'
import styles from './Login.module.scss'
import SelectOptions from './SelectOptions'

function IsLogin ({
    onShowAuthen=()=>{},
}) {
    return (
        <div className={styles.isLogin}>
           <div style={{
                display:'inline-block'
            }} onClick={onShowAuthen}>
             <Button type="normal" underline size="s">Tải lên</Button> 
            </div>
            <div style={{
                display:'inline-block'
            }} onClick={onShowAuthen}>
            <Button >Đăng Nhập</Button>  
            </div>
            <Tippy
                render={attrs => (
                <div className={styles.tippySelect} tabIndex="-1" {...attrs}>
                    <SelectOptions/>
                </div>
                )}
                interactive={true}
                delay={500}
                offset={[-90,15]}
                >
                 <div className={styles.iconEllipsisV}>
                    <FontAwesomeIcon icon={faEllipsisV} className={styles.faEllipsisV} />
                </div>
            </Tippy>
        </div>
    )
}

export default IsLogin
