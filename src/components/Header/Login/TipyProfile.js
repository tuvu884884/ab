import styles from './Login.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog, faQuestionCircle, faSignInAlt} from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-regular-svg-icons'
import { faAutoprefixer } from '@fortawesome/free-brands-svg-icons'
import { Link } from 'react-router-dom'
import config from 'config'


function TipyProfile({
    nickname= '',
    onLogOut = ()=>{},
}) {
    return (
        <div className={styles.tipyProfile}>
           <div className={styles.info}>
               <div className={styles.item}>
                    <div className={styles.icon}>
                        <FontAwesomeIcon icon={faUser}/>
                    </div>
                   <Link  to={`${config.routes.home}profile/@${nickname}`}>
                   <p className={styles.text}>
                        Xem hồ sơ
                    </p>
                   </Link>
               </div>
               <div className={styles.item}>
                    <div className={styles.icon}>
                        <FontAwesomeIcon icon={faCog}/>
                    </div>
                    <p className={styles.text}>
                        Cài đặt
                    </p>
               </div>

               <div className={styles.item}>
                    <div className={styles.icon}>
                        <FontAwesomeIcon icon={faAutoprefixer}/>
                    </div>
                    <p className={styles.text}>
                        Tiếng Việt
                    </p>
               </div>
               <div className={styles.item}>
                    <div className={styles.icon}>
                        <FontAwesomeIcon icon={faQuestionCircle}/>
                    </div>
                    <p className={styles.text}>
                        Phản hồi và Trợ giúp
                    </p>
               </div>
           </div>
           <div className={styles.login}  onClick={()=> onLogOut()}>
                    <div className={styles.icon}>
                        <FontAwesomeIcon icon={faSignInAlt}/>
                     </div>
                     <p className={styles.text}>
                        Đăng suất
                    </p>
           </div>
        </div>
    )
}

export default TipyProfile
