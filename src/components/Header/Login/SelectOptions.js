import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faIdBadge, faQuestionCircle } from '@fortawesome/free-solid-svg-icons'

import styles from './Login.module.scss'

function SelectOptions ()  {
   return (
       <>
            <div className={styles.group}>
                <div className={styles.icon}>
                    <FontAwesomeIcon icon={faIdBadge} />
                </div>
                <div className={styles.text}>
                    Tiếng Việt
                </div>
            </div>
            <div className={styles.group}>
                <div className={styles.icon}>
                    <FontAwesomeIcon icon={faQuestionCircle} />
                </div>
                <div className={styles.text}>
                    Phản Hồi và Trợ Giúp
                </div>
            </div>
       </>
   )
}

export default SelectOptions
