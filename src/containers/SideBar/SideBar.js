import Navbar, {
    FooterSidebar, TopSidebar
} from '../../components/Navbar'
import MidSidebar from './Mid'
import { actions as actionsModel} from '~/state/sateModel'
import {useSelector, useDispatch} from 'react-redux'

function SideBar () {
   const dispatch = useDispatch()
   const handleModal = ()=> {
        dispatch(actionsModel.handleShowModal())
   }
   const data = useSelector(state => state.user.currentUser)
   return (
         <Navbar >
           <TopSidebar
               showModal={handleModal}
           />
           <MidSidebar
               heading = 'Suggested accounts'
               apiPath="/api/users/suggested"
           />
                {
                     data ? <MidSidebar
                heading = 'Your top accounts'
                apiPath="/api/me/followings"
            />:null
                }
            <FooterSidebar />
         </Navbar>
   )
}

export default SideBar
