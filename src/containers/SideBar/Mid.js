
import { MidSidebar as MidSidebarComponent, MidItem } from '../../components/Navbar'
import axios from 'axios'
import { useEffect, useState, useRef } from 'react'
function MidSidebar ({
    heading = '',
    apiPath = ''
}) {
    const [Account, setAccount] = useState([])
   const [seeAll, setSeeAll] = useState(true)
   const [heighSidebar, setHeighSidebar] = useState(null)
   const [hideSeeSuggestedBtn, sethideSeeSuggestedBtn] = useState(true)

   const [pagination, setPagination] = useState({
         currentPage : 1,
         totalPages: 0,
         total:0,
         perPage: 0,
   })
      
     const handleSeeToggle =  ()=> {
     
      setSeeAll(!seeAll)
      if(seeAll) {
        setHeighSidebar(null)
        if(Account.length <=5)
          {setPagination({
            currentPage: pagination.currentPage + 1,
         })}
      } else {
          setHeighSidebar({
            height: '320px',
            overflow: 'hidden',
        })
      }
     }
     const handleLoadMore  = ()=> {
        if (Account.length <= pagination.perPage || pagination.currentPage >= pagination.totalPages) {
             return
        } else {
            setPagination({
                currentPage: pagination.currentPage + 1,
            })
        }
     }

      useEffect(() => {
          axios.get(`${apiPath}?` + pagination.currentPage)
          .then((response) => {
            setAccount([...Account,...response.data])
            setPagination({
               ...pagination,
               totalPages:response.meta.pagination.total_pages,
               total:response.meta.pagination.total,
               perPage:response.meta.pagination.per_page,
            })
          })
          .catch((error) => {

          })
      },[pagination.currentPage])

      const newData = useRef()
    const testApiToggleFollow = useRef([])
   const handleFollow = (data)=> {
    if(testApiToggleFollow.current.includes(data.id)){
        return
    }
    let apiFollow = `/api/users/${data.id}/follow`
    if(data.is_followed) {
        apiFollow = `/api/users/${data.id}/unfollow`
    }
    testApiToggleFollow.current.push(data.id)
    axios.post(apiFollow)
     .then(response=>{
         const index = Account.findIndex(post => post.id === data.id)
         newData.current = Account[index]
         newData.current.is_followed = response.data.is_followed 
         setAccount(oldposts => {
             const newposts = oldposts.slice(0)
             newposts.splice(index, 1, newData.current)
             return newposts
         })
     })
     .catch(err => console.error(err))
     .finally(() => {
         const index = testApiToggleFollow.current.indexOf(data.id)
         testApiToggleFollow.current.splice(index, 1)
     })
   }
   if(!Account) {
     return <h2>Loading....</h2>
   }
    return (
        <MidSidebarComponent 
            heading = {heading}
            see = { seeAll ? 'Xem Tất Cả':'Ẩn Bớt' }
            onSeeToggle = {handleSeeToggle}
            seeAll = {seeAll}
            hideSeeBtn = {hideSeeSuggestedBtn}
        >
           <div  style ={heighSidebar} >
           { Account.map((item, index) =>{
               return (
                   <MidItem 
                       key= {index}
                       name={item.first_name + ' ' + item.last_name}
                       nickname = {item.nickname}
                       avatar={item.avatar  }
                       isLast = {Account.length === index + 1}
                       onEnter = {handleLoadMore}
                       isFollowed= {item.is_followed}
                       followersCount={item.followers_count}
                       likesCount={item.likes_count}
                       tick = {item.tick}
                       apiPath ={apiPath}
                       handleFollow = {handleFollow}
                       data={item}
                   >
                   </MidItem>
               )
            })}
           </div>
        </MidSidebarComponent>
    )
}

export default MidSidebar
