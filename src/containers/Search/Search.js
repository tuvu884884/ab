import { useLocation } from 'react-router-dom'
import { useEffect, useState } from 'react'
import axios from 'axios'

import SearchComponent from '~/components/Search'
import { Column, Row } from "@mycv/mycv-grid";
import SideBar from "../SideBar";

function useQuery() {
   return new URLSearchParams(useLocation().search);
 }

function Search () {
    let query = useQuery();
    const [searchValue, setSearchValue] = useState(query.get('q'))
    const [usersSearch, setUsersSearch] = useState(null)
    useEffect(() => {
      axios.get(`/api/users/search?q=${searchValue}&type=less&page=1`)
      .then(response => {
          setUsersSearch(response.data)
      })
    },[searchValue])
    useEffect(() => {
      setSearchValue(query.get('q'))
    },[query.get('q')])
   if(!usersSearch) {
      return (
         <h1>Loading...</h1>
      )
   }
    return (
        <Row>
        <Column size={0} sizeTablet={5} sizeDesktop={3}>
           <SideBar />
        </Column>
        <Column size={12} sizeTablet={7} sizeDesktop={9}>
           <SearchComponent
            usersSearch = {usersSearch}
            />
        </Column>
     </Row>
    )
}

export default Search
