import styles from './Herder.module.scss'
import Logo from '../../components/Header/Logo'
import Search from '../../components/Header/Search'
import Login from '../../components/Header/Login'
import config from '~/config'

import axios from 'axios'
import { useEffect, useState, useRef } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector,useDispatch } from 'react-redux'
import { actions as actionsModel} from '~/state/sateModel'

function Herder () {
    const [valuSearch, setValuSearch] = useState('')
    const [usersSearch, setUsersSearch] = useState([])
    const [isusersSearch, setIsUsersSearch] = useState(false)
    const stateModal = useSelector(state=> state.stateModel.stateModal)
    const dispatch = useDispatch()
    const history = useHistory()
    let searchViewRef = useRef()
    const tipingTimeOutRef = useRef()
    useEffect(()=> {
        if(tipingTimeOutRef.current){
            clearTimeout(tipingTimeOutRef.current)
        }
        tipingTimeOutRef.current = setTimeout(() => {
            if(valuSearch) {
                axios.get(`/api/users/search?q=${valuSearch}&type=less&page=1`)
                .then(response => {
                   setUsersSearch(response.data)
                   if(response.data) {
                    setIsUsersSearch(true)
                   }
                })
                .catch(err => {
                    console.log(err)
                })
               }
        },300)
    },[valuSearch])
    function onchangeValuSearch (e) {
        setValuSearch(e.target.value)
        if(!e.target.value) {
        setIsUsersSearch(false)
        }
    }
    
    function CloseSearch() {
        setValuSearch('')
        setUsersSearch([])
    }
    const handleViewAllSearchResult = () => {
        setUsersSearch([])
        setIsUsersSearch(false)
        if(valuSearch) {
        history.push(`${config.routes.base}/search?q=${valuSearch}`)
        }
    }
    const handleShowAuThen = () => {
        if(stateModal) {
           dispatch(actionsModel.handleHideModal())
        } else {
            dispatch(actionsModel.handleShowModal())
        }
    }
        document.addEventListener('mousedown', (event) => {
            if(searchViewRef.current) {
                if (!searchViewRef.current.contains(event.target)) {
                    setIsUsersSearch(false)
                }
            }
       })
    const hangleLogOut = () => {
         axios.post('/api/auth/logout')
         .then(response => {
             localStorage.removeItem('token')
             window.location.reload()
         })
         .catch(error => {
             console.error(error)
         })
    }
    return (
        <header className={styles.header}>
            <Logo/>
            <Search
                usersSearch={usersSearch}
                onchangeValuSearch = {onchangeValuSearch}
                valuSearch={valuSearch}
                closeSearch= {CloseSearch}
                isusersSearch= {isusersSearch}
                handleViewAllSearchResult = {handleViewAllSearchResult}
                searchViewRef = {searchViewRef}
            />
            <Login
                handleShowAuThen = {handleShowAuThen}
                hangleLogOut= {hangleLogOut}
            />
        </header>
    )
}

export default Herder
