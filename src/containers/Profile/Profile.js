import { Column, Row } from '@mycv/mycv-grid'
import SideBar from '../SideBar'
import Profile from '../../components/Profile'
import {useParams} from "react-router-dom"
import axios from 'axios'
import {useState, useEffect,useRef} from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { actions as actionsModel} from '~/state/sateModel'
function Profiles() {
    const {nickname} = useParams()
    const [profile, setProfile] = useState([])
    const [isFirst, setIsFirst] = useState(true)
    const [videoPlay, setVideoPlay] = useState(null)
    const [isVideo,setIsVideo] = useState(true)
    const user = useSelector(state => state.user.currentUser)
    const stateModal = useSelector(state=> state.stateModel.stateModal)
    const dispatch = useDispatch()

    useEffect(() => {
        axios.get('/api/users/' + nickname)
        .then((response) => {
            setProfile(response.data)
        })
    },[profile])
    const handleMouseEnterVideo = (video) => {
        setIsFirst(false)
        setVideoPlay(video)
    }
    const checkPlaying = (video) => {
        return !!videoPlay && videoPlay.id === video.id
    }
    const handleToggleHiden = ()=>{
        setIsVideo(false)

    }
    const handleToggleShow = ()=>{
        setIsVideo(true)
    }
    const newData = useRef()
    const testApiToggleFollow = useRef([])
    const handleToggleFollow = (data,id)=> {
        if(!user) {
           if(stateModal) {
               dispatch(actionsModel.handleHideModal())
           } else {
               dispatch(actionsModel.handleShowModal())
           }
           return
       }
       if(testApiToggleFollow.current.includes(id)){
           return
       }
       let apiFollow = `/api/users/${data.id}/follow`
       if(data.is_followed) {
           apiFollow = `/api/users/${data.id}/unfollow`
       }
       testApiToggleFollow.current.push(id)
       axios.post(apiFollow)
        .then(response=>{
            newData.current = profile
            newData.current.is_followed = response.data.is_followed 
            setProfile(newData.current)
        })
        .catch(err => console.error(err))
        .finally(() => {
            const index = testApiToggleFollow.current.indexOf(id)
            testApiToggleFollow.current.splice(index, 1)
        })}
    return (
        <Row>
            <Column size={0} sizeTablet={5} sizeDesktop={3}>
            <SideBar/>
            </Column>
            <Column size={12} sizeTablet={7} sizeDesktop={9}>
                <Profile
                    profile={profile}
                    name = {profile.first_name + ' ' + profile.last_name}
                    nickname = {profile.nickname}
                    avatar = {profile.avatar}
                    desc = {profile.bio}
                    followers = {profile.followers_count}
                    followings = {profile.followings_count}
                    likes = {profile.likes_count}
                    videos= {profile.posts}
                    isFirst={isFirst}
                    isFollowed = {profile.is_followed}
                    handleMouseEnterVideo= {handleMouseEnterVideo}
                    checkPlaying= {checkPlaying}
                    isVideo = {isVideo}
                    handleToggleHiden = {handleToggleHiden}
                    handleToggleShow = {handleToggleShow}
                    handleToggleFollow= {handleToggleFollow}
                />
            </Column>
        </Row>
    )
}

export default Profiles
