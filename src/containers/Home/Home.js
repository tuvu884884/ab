import { Column, Row } from "@mycv/mycv-grid";
import SideBar from "../SideBar";
import Post from "../../components/Post";
import PostEntity from "../../components/entities/Post";
import axios from "axios";
import PostDetailModal from '../PostDetail'
import {useSelector,useDispatch} from 'react-redux'
import styles from './Home.module.scss'
import config from '~/config'
import {actions as actionsModel} from '~/state/sateModel'
import { useEffect, useState, useRef } from "react";

function Home({
   type= 'for-you'
}) {
   const [playingVideoClose, setPlayingVideoClose] = useState(false);
   const [posts, setPosts] = useState([]);
   const [isScrollDown, setIsScrollDown] = useState(true);
   const [currentPost, setCurrentPost] = useState(null)
   const [comments, setComments] = useState([])
   const [isCopied, setIsCopied] = useState(false)

   const oldScrollY = useRef(0);
   const videoRefs = useRef({});
   const _isScrollDown = useRef(true);
   const currentTime = useRef(null)
   const current = useRef(null)
   const currentContinueTime = useRef(null)
   const currentPositionPost = useRef(null)
   
   const user = useSelector(state => state.user.currentUser)
   const stateModal = useSelector(state=> state.stateModel.stateModal)
   const dispatch = useDispatch()

   useEffect(() => {
       window.history.scrollRestoration = 'manual'
       return () => {
         window.history.scrollRestoration = 'auto'
       }
   },[])
   //postAPI
   useEffect(() => {
      axios.get(`/api/posts?type=${type}&page=1`).then((response) => {
         setPosts(PostEntity.createFromList(response.data));
      });

      window.onscroll  = function () {
         if (!_isScrollDown.current && window.scrollY > oldScrollY.current) {
            _isScrollDown.current = true;
            setIsScrollDown(true);
         } else if (_isScrollDown.current && window.scrollY < oldScrollY.current) {
            _isScrollDown.current = false;
            setIsScrollDown(false);
         }
      };
      oldScrollY.current = window.scrollY;
   // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [isScrollDown]);

   const handleVideoRef = (ref, videoId) => {
      videoRefs.current[videoId] = ref;
   };
   const waypointProps = {}

   if (_isScrollDown.current) {
            waypointProps.bottomOffset = 250
   } else {
            waypointProps.topOffset = -100
   }


   const handleShowDetailPost = post => {

      const videoRef = videoRefs.current[post.id]
      scrollPostIntoView(post)
      if (videoRef) {
         videoRef.getInternalPlayer().pause()
         currentTime.current = videoRef.getCurrentTime()
     }
      setCurrentPost(()=> {
         if(newPost.current && newPost.current.id === post.id) {
            return newPost.current
         }
         return post
      })
      currentPositionPost.current = posts.findIndex(postItem => postItem.id === post.id)
      const postDetailPath = `${config.routes.base}/@${post.user.nickname}/video/${post.uuid}`
      window.history.pushState(null, document.title, postDetailPath)
  }

  const continueVideo = (ref, currentTime, id) => {
   if(current.current) return
      if (ref) {
         current.current = ref
         current.current.getInternalPlayer().currentTime = currentTime
      }
      if(!ref) {
         currentContinueTime.current = current.current.getInternalPlayer().currentTime
      }
  }

  const handleCloseDetailPost = (id) => {
   for (const [key, value] of Object.entries(videoRefs.current)) {
      if( +key === id) {
            setTimeout(() => {
               videoRefs.current[id].getInternalPlayer().currentTime = currentContinueTime.current
            },1)
      }
      else {
         videoRefs.current[id].getInternalPlayer().pause()
      }
    }
    setCurrentPost(null)
    newPost.current = null
    setPlayingVideoClose(true);
    window.history.back()
   }
   const scrollPostIntoView = post => {
      const videoRef = videoRefs.current[post.id]
      if (videoRef) {
          videoRef.getInternalPlayer().scrollIntoView({ block: 'center' })
      }
  }

   const handleNextPost = () => {
      const currentIndex = currentPositionPost.current
      const nextPost = posts[currentIndex + 1]
      setCurrentPost(nextPost)
      currentPositionPost.current++
      scrollPostIntoView(nextPost)
      setComments([])
  }
  const handlePrevPost = () => {
   const currentIndex = currentPositionPost.current
      const prvePost = posts[currentIndex - 1]
      setCurrentPost(prvePost)
      currentPositionPost.current--
      scrollPostIntoView(prvePost)
      setComments([])
  }
  // liked , disliked 
  const newPost = useRef(null)
  const doingLikes = useRef([])
   const handleToggleLiked = (data) => {
      if(!user) {
         if(stateModal) {
            dispatch(actionsModel.handleHideModal())
         } else {
            dispatch(actionsModel.handleShowModal())
         }
      }
      if (doingLikes.current.includes(data.id))
      return
  doingLikes.current.push(data.id)

        let apiPath = `/api/posts/${data.id}/like`
        if(data.is_liked) {
            apiPath = `/api/posts/${data.id}/unlike`
        }
        axios.post(apiPath)
        .then(response => {
           const index = posts.findIndex(post => post.id === data.id)
           newPost.current = PostEntity.create(response.data)
           setPosts(oldPosts=>{
           const newPosts = oldPosts.slice(0)
           newPosts.splice(index, 1,newPost.current)
               return newPosts
           })
           if(currentPost) {
              setCurrentPost(newPost.current)
           }
        })
        .catch(err => {
           console.log(err)
        })
        .finally(() => {
         const index = doingLikes.current.indexOf(data.id)
         doingLikes.current.splice(index, 1)
        })
   }
   const newData = useRef()
    const testApiToggleFollow = useRef([])
  const handleToggleFollow = (data,id)=> {
   if(!user) {
      if(stateModal) {
         dispatch(actionsModel.handleHideModal())
      } else {
         dispatch(actionsModel.handleShowModal())
      }
      return
  }
  if(testApiToggleFollow.current.includes(id)){
      return
  }
  let apiFollow = `/api/users/${data.user.id}/follow`
  if(data.user.is_followed) {
      apiFollow = `/api/users/${data.user.id}/unfollow`
  }
  testApiToggleFollow.current.push(id)
  axios.post(apiFollow)
   .then(response=>{
       const index = posts.findIndex(data => data.id === id)
       newData.current = posts[index]
       newData.current.user.is_followed = response.data.is_followed 
       setPosts(oldposts => {
           const newposts = oldposts.slice(0)
           newposts.splice(index, 1, newData.current)
           return newposts
       })
   })
   .catch(err => console.error(err))
   .finally(() => {
       const index = testApiToggleFollow.current.indexOf(id)
       testApiToggleFollow.current.splice(index, 1)
   })
  }
  const copyClipBoard = (nickname) => {
     window.navigator.clipboard.writeText(`http://localhost:3000/profile/@${nickname}`)
     setIsCopied (true)
     setTimeout(() => {
      setIsCopied(false)
     },1000)
  }

   return (
      <Row>
          {isCopied&&<div className={styles.copied}>Đã sao chép</div>}
         <Column size={0} sizeTablet={5} sizeDesktop={3}>
            <SideBar />
         </Column>
         <Column size={12} sizeTablet={7} sizeDesktop={9}>
            <Post
               posts={posts}
               setPosts = {setPosts}
               handleVideoRef = {handleVideoRef}
               isScrollDown= {isScrollDown}
               videoRefs = {videoRefs}
               handleShowDetailPost = {handleShowDetailPost}
               playingVideoClose = {playingVideoClose}
               handleToggleLiked= {handleToggleLiked}
               handleToggleFollow = {handleToggleFollow}
               type= {type}
               copyClipBoard= {copyClipBoard}
            />
         </Column>
         {currentPost && (
            <PostDetailModal
               posts={currentPost}
               onClose={handleCloseDetailPost}
               currentTime = {currentTime.current}
               continueVideo= {continueVideo}
               isFirst = {currentPositionPost.current === 0}
               isLast = {currentPositionPost.current === posts.length - 1}
               handleNextPost = {handleNextPost}
               handlePrevPost = {handlePrevPost}
               handleToggleLiked = {handleToggleLiked}
               comments={comments}
               setComments={setComments}
               handleToggleFollow= {handleToggleFollow}
               type= {type}
               copyClipBoard= {copyClipBoard}
            />
         )}
      </Row>
   );
}

export default Home;
