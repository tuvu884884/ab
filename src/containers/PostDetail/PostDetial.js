import axios from "axios"
import React, {  useEffect, useRef, useState } from "react"
import {useSelector} from 'react-redux'
import PostDetailConponent from '../../components/PostDetail'


function PostDetial({
    posts = '',
    onClose = () => { },
    continueVideo = () => { },
    handleNextPost = () => { },
    handlePrevPost = () => { },
    currentTime = '',
    isFirst = '',
    isLast = '',
    handleToggleLiked = () => {},
    comments = '',
    setComments = () => { },
    handleToggleFollow =()=>{},
    type= '',
    copyClipBoard= ()=>{},
}) {
    const [post, setPost] = useState(null)
    const [comment, setComment] = useState('')
    const [isPlay,setIsPlay] = useState(true)
    const [pagination, setPagination] = useState({
        currentPage: 1,
        total: 0,
        perPage: 0,
        totalPages: 0,
    })
    const [muted,setMuted] = useState(false)
    const user = useSelector(state => state.user.currentUser)
    const videoRef = useRef()
    const closeRef = useRef()
    const nextRef = useRef()
    const prveRef = useRef()
    const valumeRef = useRef()

    useEffect(() => {
        setPost(posts)
    }, [posts])
    useEffect(() => {
        if (!user)
            return
        axios.get(`/api/posts/${posts.id}/comments?page=${pagination.currentPage}`)
            .then((response) => {
                setComments([...comments, ...response.data])
                setPagination({
                    ...pagination,
                    total: response.meta.pagination.total,
                    perPage: response.meta.pagination.per_page,
                    totalPages: response.meta.pagination.total_pages,
                })
            })
            .catch((error) => {
                console.error(error)
            })

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [posts, pagination.currentPage])
    const handleChangText = (e) => {
        setComment(e.target.value)
    }
    const handlePostComment = () => {
        if (!comment)
            return
        axios.post(`/api/posts/${posts.id}/comments`, {
            comment
        })
            .then(response => {
                setComments(commentsOld => [response.data, ...commentsOld])
                setComment('')
            })
            .catch((error) => console.error(error))
    }
    const handleSeeComment = () => {
        if (comments.length < pagination.perPage || pagination.currentPage >= pagination.totalPages)
            return
        setPagination({
            currentPage: pagination.currentPage + 1,
        })
    }
    const handleChangePlay =(event)=>{
            const target = event.target
            if(!prveRef.current)
            {
                if(isFirst){
                    if(target===valumeRef.current|| 
                        target===valumeRef.current.children[0] || 
                        target===valumeRef.current.children[0].children[0]||
                        target===closeRef.current|| target===closeRef.current.children[0] || target===closeRef.current.children[0].children[0]||
                        target===nextRef.current || target===nextRef.current.children[0] || target===nextRef.current.children[0].children[0])
                    return
                    }
                    setIsPlay(!isPlay)
                    return
            }
            if(!nextRef.current) {
                if(target===valumeRef.current|| 
                    target===valumeRef.current.children[0] || 
                    target===valumeRef.current.children[0].children[0]||
                    target===closeRef.current|| target===closeRef.current.children[0] || target===closeRef.current.children[0].children[0]||
                    target===prveRef.current || target===prveRef.current.children[0] || target===prveRef.current.children[0].children[0])
                return
                setIsPlay(!isPlay)
                return
            }
            if(target===valumeRef.current|| 
                target===valumeRef.current.children[0] || 
                target===valumeRef.current.children[0].children[0]||
                target===closeRef.current|| target===closeRef.current.children[0] || target===closeRef.current.children[0].children[0]||
                target===prveRef.current|| target===prveRef.current.children[0] || target===prveRef.current.children[0].children[0]||
                target===nextRef.current || target===nextRef.current.children[0] || target===nextRef.current.children[0].children[0]) {
         return
      }
        setIsPlay(!isPlay)
    }
    if (!post) {
        return (
            <h1>may con lon</h1>
        )
    }
    if (!comments) {
        return (
            <h1>may con lon</h1>
        )
    }
    return (
        <PostDetailConponent
            post={posts}
            avatar={post.user.avatar}
            name={post.user.first_name + ' ' + post.user.last_name}
            nickname={post.user.nickname}
            description={post.description}
            url__video={post.file_url}
            likeCount={post.likes_count}
            sharesCount={post.shares_count}
            commentsCount={post.comments_count}
            hidePostDetails={onClose}
            continueVideo={continueVideo}
            currentTime={currentTime}
            handleNextPost={handleNextPost}
            handlePrevPost={handlePrevPost}
            isFirst={isFirst}
            isLast={isLast}
            onToggleLiked={handleToggleLiked}
            comments={comments}
            comment={comment}
            handleChangText={handleChangText}
            handlePostComment={handlePostComment}
            handleSeeComment={handleSeeComment}
            isPlay= {isPlay}
            onChangePlay = {handleChangePlay}
            closeRef = {closeRef}
            nextRef = {nextRef}
            prveRef = {prveRef}
            valumeRef = {valumeRef}
            videoRef= {videoRef}
            muted = {muted}
            setMuted = {setMuted}
            onToggleFollow = {handleToggleFollow}
            type= {type}
            copyClipBoard = {copyClipBoard}
        />
    )

}

export default PostDetial
