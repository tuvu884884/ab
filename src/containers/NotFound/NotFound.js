import React from 'react'
import { useHistory } from 'react-router'
import NotFoundComponent from '~/components/NotFound'
import config from '~/config'
function NotFound() {
    const history = useHistory()
    const handlefromHome = ()=> {
        history.push(`${config.routes.base + config.routes.home}`)
    }
    return (
        <NotFoundComponent
            handlefromHome= {handlefromHome}
        />
    )
}

export default NotFound
