import AuthenComponent from '../../components/Authen'
import React, { useState, useRef } from 'react'
import axios from 'axios'
import {useDispatch,useSelector} from 'react-redux'
import { actions as actionsModel} from '~/state/sateModel'

function Authen() {
    const [isLogin, setIsLogin] = useState(true)
    const [showLoIn, setShowLogIn] = useState(true)
    const [showSignIn, setShowSignIn] = useState(true)
    const [username, setUername] = useState ('')
    const [password, setPassword] = useState ('')
    const [errors, setErrors] = useState({})
    const [confirmPassword, setConfirmPassword] = useState('')
    const dispatch = useDispatch()
   const stateModal = useSelector(state=> state.stateModel.stateModal)
    
    const modelRef = useRef()

    const hangleToggle = ()=> {
         setIsLogin(!isLogin)
    }
    const showContactLogin= ()=> {
      setShowLogIn(!showLoIn)
    }
    const showContactSignIn= ()=> {
      setShowSignIn(!showSignIn)
      setUername('')
      setPassword('')
      setErrors({})
    }
    const handleLogin= (e)=> {
       e.preventDefault()
       axios.post('/api/auth/login',{
         email:username,
         password
       }).then(response=> {
           window.localStorage.setItem('token',response.meta.token)
           if(stateModal) {
            dispatch(actionsModel.handleShowModal())
           } else {
            dispatch(actionsModel.handleHideModal())
           }
           window.location.reload()
           setUername('')
           setPassword('')
           setErrors({})
       }).catch(err=> {
         if(err.response) {
          switch(err.response.status) {
            case 422:
              const resError = {}
              Object.keys(err.response.data.errors).forEach(filed => {
                resError[filed] = err.response.data.errors[filed][0]
              })
              setErrors(resError)
              break
            case 401:
              setErrors({
                ...errors,
                password : 'Email or password is incorrect'
              })
              break
              default:
               setErrors({
                 ...errors,
                 password : 'Please contact your system administrator ... admin@gmail.com'
               })
          }
         }
       })
    }
    const handleSign = (e) => {
          e.preventDefault()
          if(password !== confirmPassword) {
            setErrors({
              password:''
            })
            setErrors({
              ...errors,
              confirmPassword:'password incorrect'
            })
            return
          }
          axios.post('/api/auth/register',{
            type:'email',
            email:username,
            password
          }).then((response) => {
            window.localStorage.setItem('token',response.meta.token)
            if(stateModal) {
              dispatch(actionsModel.handleShowModal())
             } else {
              dispatch(actionsModel.handleHideModal())
             }
            window.location.reload()
            setUername('')
            setPassword('')
            setErrors({})
          })
          .catch((err) => {
             switch(err.response.status) {
                case 422:
                  const resError = {}
                  Object.keys(err.response.data.errors).forEach(filed => {
                    resError[filed] = err.response.data.errors[filed][0]
                  })
                  setErrors(resError)
                break;
                case 409:
                  setErrors({
                    ...errors,
                    confirmPassword : err.response.data.message
                  })
                  break;
                default:
                  setErrors({
                    ...errors,
                    confirmPassword : 'Please contact your system administrator ... admin@gmail.com'
                  })
             }
          })

    }
    const onChangeUsername= (e)=> {
      setUername(e.target.value)
      setErrors({
        ...errors,
        email:null,
      })
    }
    const onChangePassword= (e)=> {
      setPassword(e.target.value)
      setErrors({
        ...errors,
        password:null,
      })
    }
    const onChangeConfirmPassword= (e)=> {
        setConfirmPassword(e.target.value)
        setErrors({
          ...errors,
          confirmPassword:null,
        })
    }
  document.addEventListener('mousedown', (event) => {
    if (modelRef.current) {
     if(!modelRef.current.contains(event.target)){
      if(stateModal) {
        dispatch(actionsModel.handleHideModal())
       } else {
        dispatch(actionsModel.handleShowModal())
       }
     }
    }
  })
  const handleToggleModal = () => {
    if(stateModal) {
      dispatch(actionsModel.handleHideModal())
    } else {
      dispatch(actionsModel.handleHideModal())
    }
  }
    return (
          <AuthenComponent
            isLogin={isLogin}
            handle = {hangleToggle}
            showContactLogin={showContactLogin}
            showContactSignIn={showContactSignIn}
            showLogin = {showLoIn}
            showSignIn = {showSignIn}
            handleShowAuThen = {handleToggleModal}
            handleLogin = {handleLogin}
            handleSign = {handleSign}
            username = {username}
            onChangeUsername = {onChangeUsername}
            password = {password}
            onChangePassword = {onChangePassword}
            errors = {errors}
            confirmPassword = {confirmPassword}
            onChangeConfirmPassword = {onChangeConfirmPassword}
            modelRef = {modelRef}
          />  
    )
}

export default React.memo(Authen)