import { Column, Row } from '@mycv/mycv-grid'
import SideBar from '../SideBar'
import Follow from '../../components/Follow'
import HomeContainer from '~/containers/Home'
import {useSelector} from 'react-redux'

import axios from 'axios'
import { useEffect, useState } from 'react'

function Following () {
   const [suggestedAccount, setSuggestedAccount] = useState([])
   const [currentAccount, setCurrentAccount] = useState(null)
   const [isFirst, setIsFirst] = useState(true)
   const [playCurrentPage, setPlayCurrentPage] = useState(true)
   const user = useSelector(state => state.user.currentUser)

      useEffect(() => {
          axios.get('/api/users/suggested?page=1&per_page=12')
          .then((response) => {
            setSuggestedAccount(response.data)
          })
          .catch((error) => {

          })
      },[])
   
    const handleMouseEnterAccount = account => {
      setPlayCurrentPage(true)
       setIsFirst(false)
      setCurrentAccount(account)
  }
  const checkPlaying = account => {
   return !!currentAccount && currentAccount.id === account.id
  }
 
   const handleClickNewTap = () => {
      setPlayCurrentPage(false)
   }
   if(!suggestedAccount) {
     return <h2>Loading....</h2>
   }
  if(user)
  return (
     <HomeContainer
        type={"following"}
     />
  )
   return (
    <Row>
    <Column size={0} sizeTablet={5} sizeDesktop={3}>
    <SideBar/>
    </Column>
    <Column size={12} sizeTablet={7} sizeDesktop={9}>
         <Follow
            handleMouseEnterAccount = {handleMouseEnterAccount}
            isPlay = {checkPlaying}
            suggestedAccount={suggestedAccount}
            isFirst= {isFirst}
            playCurrentPage= {playCurrentPage}
            handleClickNewTap = {handleClickNewTap}
         />
    </Column>
</Row>
   )
}

export default Following
