import axios from 'axios'
import {useEffect} from 'react'
function UserProvider({
    setCurrentUser=''
}) {
    const token = localStorage.getItem('token')
    useEffect(() => {
       if(token) {
           axios.get('/api/auth/me')
           .then((response) => {
            setCurrentUser(response.data)
           })
       }
    },[setCurrentUser])
    return (
        null
    )
}

export default UserProvider
