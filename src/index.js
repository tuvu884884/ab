import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App'
import './accssets/styles/global.scss'
import axios from 'axios'
import {Provider} from 'react-redux'
import store from '~/store'
axios.defaults.baseURL = 'https://tiktok.f8team.dev'
axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.localStorage.getItem('token')

axios.interceptors.response.use(function (response) {
    return response.data;
}, function (error) {
    return Promise.reject(error);
});

ReactDOM.render(
  <React.StrictMode>
     <Provider store={store}>
       <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
