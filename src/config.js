import { updateLocale } from "moment"

const config = {
    mainWidth: 1100,
    routes : {
       base:process.env.REACT_APP_PUBLIC_URL,
       home: '/',
       search: '/search',
       following: '/following',
       profile:'/profile/:nickname',
       postDetail:'/:nickname/video/:videoID',
       upload:'/upload',
       message:'/message'
    },
    socials: {
        shares: {
            whatsapp: url => `https://api.whatsapp.com/send/?text=${encodeURIComponent(url)}`,
            facebook: url => `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(url)}`,
            twitter: url => `https://twitter.com/intent/tweet?refer_source=${encodeURIComponent(url)}`,
        }
    }

}

export default config
