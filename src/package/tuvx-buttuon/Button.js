// import { Link } from 'react-router-dom'
import styles from './Button.module.scss'
import React from 'react'

function Button ({
    to = '',
    href = '',
    type="primary",
    underline = false,
    onNewTab = false,
    size="m",
    children = null,
    following = null,
    onClick = () => {}

}) {
   const classNames = [
      styles[following],
      styles[size],
      styles.wrapper,
      styles[type],
      underline ? styles.underline : ''
   ]
   let Component = 'button'
   let props = {}
   if(href) {
      Component = 'a'
      props.href = href
   }
    return (
     <Component {...props} className={classNames.join(' ')} onClick={onClick}>
        <span>{children}</span>
     </Component>
    )
}

export default React.memo(Button)
