import { combineReducers } from 'redux'
import userReducer from '~/state/user'
import staeModelReducer from '~/state/sateModel'

const reducers = {
    user: userReducer,
    stateModel:staeModelReducer,
}

const rootReducer = combineReducers(reducers)

export default rootReducer
